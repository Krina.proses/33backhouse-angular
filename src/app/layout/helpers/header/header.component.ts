import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { Router } from '@angular/router';
import { GlobalConstants } from 'src/app/services/core/global-constants';
import { LocalStorageService } from 'src/app/services/core/local-storage.service';
import { SidenavService } from 'src/app/services/core/sidenav.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit {
  @Output() eventforchild: EventEmitter<any> = new EventEmitter();
  @Input() isHorizontal:boolean
  constructor(
    private _ls: LocalStorageService,
    private _route: Router,
    private _sidebarService: SidenavService,
    private _userService: UserService,

  ) {
  }

  sidebarClosed$ = this._sidebarService.sidebarClosed$;
  status: boolean = false;
  company: { name: string; logo: string } = { name: '', logo: '' };
  userData: any = this._userService.getCurrentUser();


  headerOptions = [
    {
      name: this.userData?.email,
      icon: 'fa fa-user',
    },

    {
      name: 'logout',
      icon: 'fa fa-sign-out',
    },
  ];
  ngOnInit(): void {


  }

  clickevent() {
    this.status = !this.status;
    this.eventforchild.emit(this.status);
  }

  toggleSidebar() {
    this._sidebarService.toggleSidebar();
  }

  handleMenuChange(e) {
    if (e.name == 'logout') {
      this._logout();
    }
    else if (e.name == 'Change Password') {
      this._route.navigate(['/changePassword']);
    }
  }

  private _logout() {
    for (let key in GlobalConstants.lsKeys) {
      this._ls.remove(GlobalConstants.lsKeys[key]);
    }

    this._route.navigate(['/login']);
  }


}
