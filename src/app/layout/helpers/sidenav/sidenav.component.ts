import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {  NavigationStart, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { SidenavService } from '../../../services/core/sidenav.service';
import {matchSorter} from 'match-sorter'

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SidenavComponent implements OnInit {
  sidebar: any[] = [];
  original:any[] = []
  flatSidebar: any[] = [];
  activeMenu: string = '';
  family: number[] = [];
  searchMenu: any[] = [];
  searchTerm: string = ''
  mobileRegex = /android|iphone|kindle|ipad/i;
  isMobile = this.mobileRegex.test(navigator.userAgent);

  constructor(private _router: Router, private _ss: SidenavService) {}

  activeURL: string = '';
  sidebarClosed$ = this._ss.sidebarClosed$

  ngOnInit(): void {
    this.activeURL = this._router.url;
    this.sidebar = this._ss.getPermittedMenu();
    this.original = this.sidebar;
    this.flatSidebar = this._ss.getFlatMenu() || []
    this.searchMenu = this.flatSidebar
    .filter(x => x.link)
    .map(x => {
      let {parentID, ...rest} = x;
      return {...rest, children: []}
    })

    this.persistMenuState(this.activeURL)

    this._router.events
      .pipe(filter((event) => event instanceof NavigationStart))
      .subscribe((data: any) => (this.activeURL = data.url));
  }

      getAllParent(parentID: number) {
        let arr = this.flatSidebar;
      let index = arr.findIndex(item => item.id == parentID);
      if(arr[index] && arr[index].parentID){
        this.getAllParent(arr[index].parentID)
      }

        this.family.unshift(parentID)
    }

    persistMenuState(url: string) {
      for(let item of this.flatSidebar) {
          if(item.link == url) {
            this.getAllParent(item.parentID);
            break;
          }
      }
    }

    /** START menu click fun */
    onChildClick(item: any) {
      if (item.link) {
        this.activeMenu = item.link;

        this._router.navigate([item.link]);
        if(this.isMobile) {
          this._ss.toggleSidebar();
        }
      }
      if (!item.parentID) {
        this.family = [];
      }
    }

    onParentClick(item: any) {
      let arr = this.family;
      let index = arr.findIndex((i: any) => i == item.id);
      if (index < 0) {
        if (!item.parentID) {
          arr = [];
        }
        arr.push(item.id);
      } else {
        let num = arr.length - index;
        if (num > 0) {
          for (let i = 0; i < num; i++) {
            arr.pop();
          }
        } else {
          arr.pop();
        }
      }
      this.family = arr;
    }

    checkFamily(id: number): Boolean {
      return this.family.includes(id);
    }

    checkParentActive(id: number): Boolean {
      if (id == this.family[this.family.length - 1]) {
        return true;
      }
      return false;
    }

    resetSearch() {
      this.searchTerm = ''
      this.sidebar = this.original;
    }

    searchMenuFunc(event) {
      if(!event.target.value) {
        this.sidebar = this.original
        return
      }
      let filteredMenu = matchSorter(this.searchMenu, event.target.value, {keys: ['title']});
      this.sidebar = filteredMenu

    }

}
