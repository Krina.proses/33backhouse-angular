import { Component, OnInit } from '@angular/core';
import { SidenavService } from 'src/app/services/core/sidenav.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {

// for Horizontal navbar isHorizontal = true, for Vertical navbar isHorizontal = false

  public isHorizontal = false

  public status: any;
  constructor(private _ss: SidenavService,
  ) {


  }
  sidebarClosed$ = this._ss.sidebarClosed$

  parentFunction(data: any) {
    this.status = data;
  }
  ngOnInit(): void {


  }


}
