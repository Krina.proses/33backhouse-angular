import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './layout/main/main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidenavComponent } from './layout/helpers/sidenav/sidenav.component';
import { HorizontalSidenavComponent } from './layout/helpers/horizontal_sidenav/horizontal_sidenav.component';

import { HeaderComponent } from './layout/helpers/header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AuthModule } from './modules/auth/auth.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenAuthInterceptor } from './services/core/token-auth.interceptor';
import { ToastrModule } from 'ngx-toastr';
import { AuthGuard } from './services/core/auth.guard';
import { DashMenuModule } from './shared/ui/menu/menu.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { ActivityLoggerModule } from './modules/activity-logger/activity-logger.module';

import { NgSelectModule } from "@ng-select/ng-select";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from './shared/shared.module';
import { IsccmLoaderModule } from './shared/ui/loader/loader.component';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    SidenavComponent,
    HeaderComponent,
    HorizontalSidenavComponent

  ],
  imports: [
    ActivityLoggerModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AuthModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    CommonModule,
    HttpClientModule,
    DashMenuModule,
    NgSelectModule,
    NgbModule,
    IsccmLoaderModule,
    ToastrModule.forRoot(),
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts'),
    }),
  ],
  providers: [
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: TokenAuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

//
