import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './layout/main/main.component';
import { AuthGuard } from './services/core/auth.guard';
import { RouterpermissionGuard } from './services/core/routerpermission.guard';
const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./modules/dashboard/dashboard.module').then(
            (m) => m.DashboardModule
          ),
        canActivate: [AuthGuard, RouterpermissionGuard],
      },

      {
        path: 'user-management',
        loadChildren: () =>
          import('./modules/user-management/user-management.module').then(
            (m) => m.UserManagementModule
          ),
        canActivate: [AuthGuard, RouterpermissionGuard],
      },
      {
        path: 'activity-logger',
        loadChildren: () =>
          import('./modules/activity-logger/activity-logger.module').then(
            (m) => m.ActivityLoggerModule
          ),
        canActivate: [AuthGuard, RouterpermissionGuard],
      },

      {
        path: 'location',
        loadChildren: () =>
          import('./modules/location/location.module').then(
            (m) => m.LocationModule
          ),
        canActivate: [AuthGuard, RouterpermissionGuard],
      },

      {
        path: 'master/category',
        loadChildren: () =>
          import('./modules/master/category/category.module').then(
            (m) => m.CategoryModule
          ),
        canActivate: [AuthGuard, RouterpermissionGuard],
      },
      {
        path: 'master/attribute',
        loadChildren: () =>
          import('./modules/master/attribute/attribute.module').then(
            (m) => m.AttributeModule
          ),
        canActivate: [AuthGuard, RouterpermissionGuard],
      },
      {
        path: 'master/item',
        loadChildren: () =>
          import('./modules/master/item/item.module').then(
            (m) => m.ItemModule
          ),
        canActivate: [AuthGuard, RouterpermissionGuard],
      },
    ],
  },

  {
    path: 'auth',
    loadChildren: () =>
      import('./modules/auth/auth.module').then((m) => m.AuthModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
