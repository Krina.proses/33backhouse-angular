import { Directive, HostListener, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[onlyNumber]'
})
export class NumberOnlyDirective {

  constructor(private el: ElementRef) { }

  @Input() onlyNumber: boolean = false;
  @Input() allowDot: boolean = false;


  @HostListener('keypress', ['$event']) onKeypress(event) {
    const e = <KeyboardEvent>event;
    if (this.onlyNumber) {
      const charCode = (event.which) ? event.which : event.keyCode;
      if ((charCode >= 48 && charCode <= 57) || charCode === 8 || (this.allowDot && charCode == 46)) {

        return true;
      } else {
        e.preventDefault();
      }
    }
  }

  @HostListener('input', ['$event']) onInput($event) {
    if (this.allowDot) {
      $event.target.value = $event.target.value.replace(/[^.\d]/g, '').replace(/^(\d*\.?)|(\d*)\.?/g, '$1$2');
    } else {
      $event.target.value = $event.target.value.replace(/\D/g, '');

    }
    $event.preventDefault();
  }
}
