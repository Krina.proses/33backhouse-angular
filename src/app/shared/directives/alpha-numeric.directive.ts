import { Directive, HostListener, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[alphaNumeric]'
})
export class AlphaNumericDirective {

  constructor(private el: ElementRef) { }

  @Input() alphaNumeric: boolean;
  @Input() upperCase: boolean;

  @HostListener('keypress', ['$event']) onKeypress(event) {
    const e = <KeyboardEvent>event;
    if (this.alphaNumeric) {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (
        (charCode > 96 && charCode < 123) ||
        (charCode > 64  && charCode < 91) ||
        (charCode >= 48 && charCode <= 57 ) ||
        charCode === 8 ) {
        return true;
      } else {
        e.preventDefault();
      }
    }
  }

  @HostListener('input', ['$event']) onInput($event) {
    if (this.upperCase) {
      const start = $event.target.selectionStart;
      const end = $event.target.selectionEnd;
      $event.target.value = $event.target.value.toUpperCase();
      $event.target.setSelectionRange(start, end);
      $event.preventDefault();
    }
  }
}
