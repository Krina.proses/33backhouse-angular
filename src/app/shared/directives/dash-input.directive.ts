import {
  Directive,
  ElementRef
} from '@angular/core';

@Directive({
  selector: 'input[dashInput]',
  host: {
    class: 'form-control',
  },
})
export class DashInputDirective {
  constructor(
    protected _elementRef: ElementRef<
      HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement
    >
  ) {
    const element = this._elementRef.nativeElement;
  }
}
