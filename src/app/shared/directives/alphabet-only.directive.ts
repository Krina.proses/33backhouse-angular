import { Directive, HostListener, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[alphabetOnly]'
})
export class AlphabetOnlyDirective {

  constructor(private el: ElementRef) { }

  @Input() alphabetOnly: boolean;

  @HostListener('keypress', ['$event']) onKeypress(event) {
    const e = <KeyboardEvent>event;
    if (this.alphabetOnly) {
      const charCode = (event.which) ? event.which : event.keyCode;
      if ((charCode > 96 && charCode < 123) || charCode === 32 || (charCode > 64  && charCode < 91)) {
        return true;
      } else {
        e.preventDefault();
      }
    }
  }
}
