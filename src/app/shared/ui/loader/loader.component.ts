import { ChangeDetectionStrategy, Component, NgModule } from '@angular/core';
import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';
import { CommonModule } from '@angular/common';
import { LoaderService } from './loader.service';

@Component({
  selector: 'isccm-loader',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./loader.component.scss'],
  animations: [
    trigger('fadeIn', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [style({ opacity: 0 }), animate(350)]),
      transition(':leave', animate(200, style({ opacity: 0 }))),
    ]),
  ],
  template: `
    <div class="spinner-overlay" [@fadeIn]="'in'" *ngIf="ls.loader$ | async">
      <div class="gridLoader">
        <div class="custom-loader">
        <img src="assets/img/favicon.png" alt="33BAKEHOUSE" class="openLogo" />
        </div>
        <div class="loaderText">Loading...</div>
      </div>
      <div></div>
    </div>
  `,
})
export class IsccmLoaderComponent {
  constructor(public ls: LoaderService) {}
}

@NgModule({
  imports: [CommonModule],
  exports: [IsccmLoaderComponent],
  declarations: [IsccmLoaderComponent],
})
export class IsccmLoaderModule {}
