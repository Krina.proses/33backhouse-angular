import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-button',
  template: `<button
    (click)="onClick.emit($event)"
    [type]="type"
    [disabled]="isDisabled"
    class="mx-1 {{ finalClass }}"
  >
    <span id="wrapper">
      <i [class]="finalIcon" *ngIf="finalIcon && iconPosition == 'left'"></i
      ><span *ngIf="finalText && finalIcon && iconPosition == 'left'"
        >&nbsp;</span
      >{{ finalText
      }}<span *ngIf="finalText && finalIcon && iconPosition == 'right'"
        >&nbsp;</span
      ><i [class]="finalIcon" *ngIf="finalIcon && iconPosition == 'right'"></i>
    </span>
  </button>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  // @Input() id: string | number = '';
  @Input() class: string = ''; // if want to pass ectra property
  @Input() type: string = 'button'; // for button type
  @Input() isDisabled: boolean = false; // toggle disable event
  @Input() loading: boolean = false; // toggle loading
  @Input() color:
    | 'default'
    | 'primary'
    | 'secondary'
    | 'success'
    | 'info'
    | 'warning'
    | 'danger'
    | 'light'
    | 'link'
    | 'dark' = 'primary'; // button color
  @Input() outlined: boolean = false; // to handle outlined button

  @Input() size: 'large' | 'small' = 'small'; // to handle six of the button

  @Input() icon: string = ''; // if want to pass icon
  @Input() iconPosition: 'left' | 'right' = 'left'; // to define icon possition
  @Input() text: string = ''; // text of the button

  @Output() onClick: any = new EventEmitter<any>(); // execution prop

  public finalClass = this.class;
  public finalText = this.text;
  public finalIcon = this.icon;
  loadingText = 'Loading...';
  loadingIcon = 'fas fa-spinner fa-spin';
  addIcon = 'fa-solid fa-plus';
  backIcon = 'fa-solid fa-angle-left';
  // backIcon = 'fa-solid fa-backward-step';/'fa-solid fa-angles-left';
  sendIcon = 'fa-solid fa-paper-plane';

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['loading']) {
      this.finalText = !this.loading ? this.text : this.loadingText;
      this.finalIcon = !this.loading ? this.icon : this.loadingIcon;
      if (changes && changes['disabled'] && changes['disabled'].currentValue) {
        this.isDisabled = true;
      } else {
        this.isDisabled = false;
        if (changes['loading'].currentValue) {
          this.isDisabled = true;
        } else {
          this.isDisabled = false;
        }
      }
    }

    if (changes['text']) {
      this.finalText = !this.loading ? this.text : this.loadingText;
    }

    if (changes['icon']) {
      this.finalIcon = !this.loading ? this.icon : this.loadingIcon;
    }
    this.finalClass = ''

    if (!this.outlined) {
      this.finalClass = this.finalClass.concat(
        '',
        this.size == 'large'
          ? `btn btn-${this.color} btn-lg`
          : `btn btn-${this.color} btn-sm`
      );
    } else {
      this.finalClass = this.finalClass.concat(
        this.size == 'large'
          ? `btn btn-outline-${this.color} btn-lg`
          : `btn btn-outline-${this.color} btn-sm`
      );
    }
  }
}
