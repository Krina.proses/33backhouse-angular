import { Pipe, PipeTransform } from '@angular/core';
import { OPTION_TITLES } from '../data-grid.type';

@Pipe({
  name: 'idgen'
})
export class IdGeneratorPipe implements PipeTransform {
  transform(value: string): string {
    return value.toLocaleLowerCase().split(' ').join('-')
  }

}
