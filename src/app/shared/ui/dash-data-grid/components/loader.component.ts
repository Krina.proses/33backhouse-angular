import { ChangeDetectionStrategy, Component } from "@angular/core";


@Component({
  selector: 'grid-loader',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<div class="gridLoader">
    <div class="custom-loader">
    <img src="assets/img/favicon.png" alt="33BAKEHOUSE" class="openLogo" />
    </div>
    <div class="loaderText">Loading...</div>
  </div>`
})

export class GridLoader {

}
