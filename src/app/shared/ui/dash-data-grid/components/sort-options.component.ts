import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { DataGridService } from '../data-grid.service';
import { GRID_OPTIONS, headerObject } from '../data-grid.type';

@Component({
  selector: 'sort-options',
  template: `
    <i
      class="fa-solid fa-arrow-up pointer mx-2"
      (click)="handleChange(2)"
      *ngIf="activeSortIndex === 1"
    ></i>
    <i
      class="fa-solid fa-arrow-down pointer mx-2"
      (click)="handleChange(1)"
      *ngIf="activeSortIndex === 0"
    ></i>
    <i
      class="fa-solid fa-minus pointer mx-2"
      (click)="handleChange(0)"
      *ngIf="activeSortIndex === 2"
    ></i>
  `,
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SortOptionsComponent implements OnChanges {
  constructor(public ds: DataGridService) {}

  @Input() header: headerObject;
  @Input() activeSort: string;
  @Output() handleClick = new EventEmitter();

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes['activeSort'].currentValue &&
      changes['activeSort'].currentValue !== this.header.name
    ) {
      this.activeSortIndex = 2;
    }
  }
  options = [
    { name: 'sort ASC', type: GRID_OPTIONS.SORT, value: 'ASC' },
    { name: 'sort DESC', type: GRID_OPTIONS.SORT, value: 'DESC' },
    { name: 'unsort', type: GRID_OPTIONS.SORT, value: undefined },
  ];

  activeSortIndex = 2;

  shouldIRender(activeS) {}

  handleChange(e) {
    this.activeSortIndex = e;
    this.handleClick.emit({
      action: this.options[e],
      header: this.header,
    });
  }
}
