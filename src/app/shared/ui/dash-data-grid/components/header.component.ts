import { CdkDragDrop } from '@angular/cdk/drag-drop';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { DataGridService } from '../data-grid.service';
import { GRID_OPTIONS, headerObject } from '../data-grid.type';

@Component({
  selector: `[grid-header]`,
  template: `
    <tr class="example-list">
      <th *ngIf="checkbox" class="tblCheckbox">
        <input
          class="head-checkbox"
          type="checkbox"
          [checked]="
            (ds.allSelected$ | async) && !(ds.excludedEntries$ | async).length
          "
          (change)="ds.toggleSelectAll($event)"
        />
      </th>
      <ng-container
        *ngFor="let header of ds.headers$ | async; let head_index = index"
      >
        <th
        [ngStyle]="{ 'width': header.customWidth  }"
          id="headerID"
          *ngIf="header.visible"
          [class]="header.headerClass | render"
          [id]="header.title | idgen"
          resizer
        >
          <span class="d-flex justify-content-between align-items-center">
            <span class="headerTitle">{{ header.title }}</span>
            <sort-options
            [header]="header"
            (handleClick)="handleClick($event)"
            [activeSort]="activeSort"
            ></sort-options>
          </span>
          <span
            class="resizer"
            [id]="(header.title | idgen) + 'resizer-node'"
          ></span>
        </th>
      </ng-container>
      <th class="actionCol text-center" *ngIf="ds.hasActions()">
        <span>Actions</span>
      </th>
    </tr>
    <tr class="filteropen" *ngIf="ds.showfilter$ | async">
    <th class="multiselect-filter-area"></th>
      <ng-container
        *ngFor="let header of ds.headers$ | async; let head_index = index"
      >
        <th *ngIf="header.visible && !header.filter"></th>
        <th
          grid-filter
          *ngIf="header.visible && header.filter"
          [config]="header.filter"
          [headerKey]="header.name"
        ></th>
      </ng-container>
      <th *ngIf="ds.hasActions()"></th>
    </tr>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['../data-grid.component.scss'],
})
export class GridHeaderComponent {
  constructor(public ds: DataGridService) { }
  @Output() onDrag = new EventEmitter<CdkDragDrop<string[]>>();
  @Input() checkbox: boolean;

  drop(event: CdkDragDrop<string[]>) {
    this.onDrag.emit(event);
  }

  activeSort: string | null = ''

  handleClick({ action, header }) {
    this.activeSort = header.name
    this.ds.handleHeaderOptions(action, header);
  }
}
