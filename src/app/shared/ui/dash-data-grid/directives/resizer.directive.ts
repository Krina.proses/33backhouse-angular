import {
  AfterViewInit,
  Directive,
  ElementRef,
  OnDestroy,
  OnInit,
  Renderer2,
} from '@angular/core';
import { DataGridService } from '../data-grid.service';

@Directive({
  selector: '[resizer]',
})
export class ResizerDirective implements AfterViewInit, OnDestroy {
  constructor(
    private renderer: Renderer2,
    private el: ElementRef,
    private _ds: DataGridService
  ) { }

  resizer;
  column;
  mouseMoveListener;
  mouseUpListener;
  mouseDownListener;

  ngOnDestroy() {
    this.mouseDownListener();
  }

  ngAfterViewInit() {
    // this.calculateTableWidth();
    this.column = this.el.nativeElement as HTMLTableColElement;
    this.resizer = this.column.childNodes[1];
    let columnWidth: any
    setTimeout(() => {
      columnWidth = this.el.nativeElement.style.width
      this.updateColWidth(parseInt(columnWidth));
      this._ds.triggerTableWidthRecalculate('col');

      this.mouseDownListener = this.renderer.listen(
        this.resizer,
        'mousedown',
        (event) => {
          const resizerWidth = event.clientX;
          // this.renderer.listen
          this.mouseMoveListener = this.renderer.listen(
            'document',
            'mousemove',
            (mouseMoveEvent) => {
              const dx = mouseMoveEvent.clientX - resizerWidth + parseInt(columnWidth);
              this.updateColWidth(dx);
              this._ds.triggerTableWidthRecalculate('col');
            }
          );

          this.mouseUpListener = this.renderer.listen(
            'document',
            'mouseup',
            () => {
              this.mouseMoveListener();
              this.mouseUpListener();
            }
          );
        }
      );
    }, 100);

  }

  // calculateTableWidth() {
  //   this.column = this.el.nativeElement as HTMLTableColElement;
  //   const width = this.column.offsetHeight;
  //   this._ds.addToTableWidth(width);
  //   this.resizer = this.column.childNodes[1];
  // }

  updateColWidth(width: number) {
    this.renderer.setStyle(this.column, 'width', `${width}px`);

    // this._ds.addToTableWidth(width);
  }
}
