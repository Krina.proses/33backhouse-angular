import {
  AfterViewInit,
  Directive,
  ElementRef,
  OnDestroy,
  Renderer2,
} from '@angular/core';
import { DataGridService } from '../data-grid.service';

@Directive({
  selector: '[tableWidth]',
})
export class TableWidthDirective implements OnDestroy, AfterViewInit {
  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
    private ds: DataGridService
  ) { }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.widthsub.unsubscribe()
  }

  tableWrapperWidth = null;
  table = this.el.nativeElement as HTMLTableElement;


  ngAfterViewInit() {
    const wrapper = this.table.parentElement;
    const computedWidth = window.getComputedStyle(wrapper).width;
    this.tableWrapperWidth = parseInt(computedWidth);
    const columns = this.table.querySelectorAll('.example-list th');

    this._updateTableWidth();
    this.updateColWidth(this.tableWrapperWidth, columns.length);

  }

  sub = this.ds.triggerTableWidthRecalculate$.subscribe((col) => {
    if (col) {
      this.calculateTableWidth();
    }
  });

  widthsub = this.ds.tabelUpdateColwidth$.subscribe((col) => {
    if (col) {
      const columns = this.table.querySelectorAll('.example-list th');
      this.updateColWidth(this.tableWrapperWidth, columns.length);
    }
  });

  calculateTableWidth() {
    const columns = this.table.querySelectorAll('.example-list th');

    const totalWidth = [].reduce.call(
      columns,
      (sum, col) => {
        const w = parseInt(col.style.width);

        if (w) {
          return sum + w;
        }
        return sum;
      },
      0
    );

    this._updateTableWidth(totalWidth);
  }

  private _updateTableWidth(w?: number) {
    if (w && w > this.tableWrapperWidth) {
      this.renderer.setStyle(this.table, 'width', `${w}px`);
      return;
    }
    this.renderer.setStyle(this.table, 'width', `${this.tableWrapperWidth}px`);
  }

  updateColWidth(width: number, row: number) {

    if (row > 12) {
      row = 10
    }
    const perColWidth = Math.floor(width / (row))
    this.table.querySelectorAll('th').forEach((item, index, array) => {
        return item.style.width = (perColWidth) + 'px'

    });

  }
}
