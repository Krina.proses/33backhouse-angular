import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, catchError, tap } from 'rxjs';
import { ApiService } from 'src/app/services/core/api.service';
import { SanitizationService } from 'src/app/services/core/sanitization.service';
import { SubSink } from 'subsink';
import { DataGridService } from '../dash-data-grid/data-grid.service';
import {
  actionButtonType,
  filterEmitterType,
} from '../dash-data-grid/data-grid.type';
import { ShellConfig } from './grid-shell.types';

@Component({
  selector: 'dash-grid-shell',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `<dash-data-grid
    [headerInfo]="headers"
    [data]="source"
    [skipPermission]="skipPermission"
    [filterDropdownData]="filterDropdownData"
    (handleChange)="handleChange($event)"
    [totalCount]="totalCount"
    [loading]="loading"
    [actionButtons]="actionButtons"
    (actionButtonClick)="actionButtonClick.emit($event)"
    [bulkActions]="bulkActions"
    (bulkActionClick)="bulkActionClick.emit($event)"
    (addClick)="addClick.emit($event)"
    [checkbox]="checkbox"
    [excelURL]="excelURL"
    [extraExportKeys]="extraExportKeys"
    [handleExport]="handleExport"
    [selected]="selected"
    [headerTitle]="headerTitle"
    [id]="router.url"
    [showAdd]="showAdd"
    [extraButtons]="extraButtons"
    (extraButtonClick)="extraButtonClick.emit($event)"
  ></dash-data-grid>`,
})
export class GridShellComponent implements OnInit, OnDestroy {
  constructor(
    private _api: ApiService,
    private _sanitizer: SanitizationService,
    private _cdr: ChangeDetectorRef,
    private _ds: DataGridService,
    public router: Router
  ) {}

  @Input() config: ShellConfig = null;
  @Input() filterDropdownData: any;
  @Input() actionButtons: actionButtonType[];
  @Input() bulkActions: string[];
  @Input() checkbox: boolean = true;
  @Input() excelExport: boolean | string = false;
  @Input() handleExport: any;
  @Input() selected: any[];
  @Input() extraExportKeys: { key: string; title: string }[];
  @Input() headerTitle: string;
  @Input() showAdd: boolean;
  @Input() skipPermission: boolean;
  @Input() extraButtons: string[];

  @Output() actionButtonClick = new EventEmitter<any>();
  @Output() bulkActionClick = new EventEmitter();
  @Output() extraButtonClick = new EventEmitter();
  @Output() addClick = new EventEmitter();

  source: any[] = [];
  filterSubject = new BehaviorSubject<filterEmitterType>(null);
  filterChange$ = this.filterSubject.asObservable();
  private _subs = new SubSink();
  loading: boolean = false;
  totalCount: number = null;
  private debounce: any;

  get excelURL() {
    if (typeof this.excelExport == 'string') {
      return this.excelExport;
    } else if (typeof this.excelExport == 'boolean' && this.excelExport) {
      return this.config.endpoint;
    }
    return '';
  }

  get headers() {
    return this.config.headers;
  }

  ngOnInit(): void {
    this._subs.sink = this.filterChange$.subscribe((data) => {
      this._getData(data);
    });
  }

  ngOnDestroy() {
    this._subs.unsubscribe();
  }

  private _getData(qp: filterEmitterType) {
    if (!qp) return;

    if (!this.config.endpoint) return;

    this.loading = true;
    this._cdr.detectChanges();
    let q = {
      filters: this._sanitizer.sanitizeFilterObject(qp.filters),
      limit: qp.limit,
      sort: this._sanitizer.sanitizeFilterObject(qp.sort),
      page: qp.page,
    };

    this._api
      .get(this.config.endpoint, q)
      .pipe(
        tap(() => {
          this.loading = false;
        }),
        catchError((err) => {
          this.loading = false;
          return err;
        })
      )
      .subscribe((data) => {
        this.totalCount = data.data.count;
        this.source = data.data.rows;
        this._cdr.detectChanges();
      });
  }

  handleChange(e) {
    if (!e) return;
    if (this.debounce) {
      clearTimeout(this.debounce);
    }

    let delay = 0;
    if (e.latestChange == 'filters') {
      delay = 500;
    }

    this.debounce = setTimeout(() => {
      let { latestChange, ...rest } = e;
      this.filterSubject.next(rest);
    }, delay);
  }

  refresh() {
    let filters = this.filterSubject.getValue();

    // Pratik ==> need to discuss same api called twice for conclave component
    // if (filters) {
    //   this._getData(filters);
    // }
    this._ds.resetSelection();
  }
}
