import { Component, Input, OnInit } from '@angular/core';
import { PatchService } from 'src/app/services/patch.service';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss'],
})
export class PageHeaderComponent implements OnInit {
  @Input() headerTitle: string = '';

  constructor(private ps: PatchService) {}

  ngOnInit(): void {

  }
}
