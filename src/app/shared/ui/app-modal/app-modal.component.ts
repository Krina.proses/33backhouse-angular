import { Component, EventEmitter, Input, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { Dialog, DialogRef, DIALOG_DATA } from '@angular/cdk/dialog';


@Component({
  selector: 'app-modal',
  templateUrl: './app-modal.component.html',
  styleUrls: ['./app-modal.component.scss'],

})
export class AppModalComponent {
  constructor(public dialog: Dialog,
  ) { }
  @ViewChild('template', { static: false }) template: any;
  @Input() modelWidth: any
  @Input() modelHeight: any

  @Input() headerTitle: any
  @Input() hideHeader: false
  @Output() afterClose = new EventEmitter();

  public dialogRef: DialogRef
  public show(): void {
    this.dialogRef = this.dialog.open<any>(this.template, {
      width: this.modelWidth,
      height:this.modelHeight,
      disableClose: true
    });

    this.dialogRef.closed.subscribe(result => {
    });
  }
  public close() {
    this.dialogRef.close();
    this.afterClose.emit();

  }
  public submit() {

  }
  public hide() {
    this.dialogRef.close();
    this.afterClose.emit();

  }
}
