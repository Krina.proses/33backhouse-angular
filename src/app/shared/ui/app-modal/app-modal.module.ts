import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { AppModalComponent } from './app-modal.component';
import { FormsModule } from '@angular/forms';
import { DialogModule } from '@angular/cdk/dialog';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [AppModalComponent],
  imports: [NgSelectModule, CommonModule, FormsModule, DialogModule],
  exports: [AppModalComponent],
  providers:[DatePipe]

})
export class AppModalModule { }
