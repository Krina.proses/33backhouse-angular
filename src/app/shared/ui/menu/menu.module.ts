import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CdkMenuModule} from '@angular/cdk/menu';
import { DashMenuComponent } from './menu.component';

@NgModule({
  declarations: [DashMenuComponent],
  imports: [CommonModule, CdkMenuModule],
  exports: [DashMenuComponent],
})
export class DashMenuModule {}
