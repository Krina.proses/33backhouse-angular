import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'dash-menu',
  template: ` <span [cdkMenuTriggerFor]="menu" class="{{ triggerParentClass }}">
      <ng-content> </ng-content>
    </span>

    <ng-template #menu>
      <div class="example-menu {{ class }}" cdkMenu>
        <button
          class="example-menu-item {{ option.class }}"
          *ngFor="let option of options"
          (click)="handleChange.emit(option)"
          cdkMenuItem
        >
          <i [class]="option.icon" *ngIf="option.icon"></i>
          {{ option[bindLabel] }}
        </button>
      </div>
    </ng-template>`,
  styleUrls: ['./menu.style.scss'],
})
export class DashMenuComponent implements OnInit {
  @Input() options: any[] = [];
  @Input() bindLabel: string | number = '';
  @Input() class: string = '';
  @Input() triggerParentClass: string = '';

  @Output() handleChange = new EventEmitter<any>();

  ngOnInit(): void {}
}
