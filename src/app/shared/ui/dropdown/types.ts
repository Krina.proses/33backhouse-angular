export type apiDataType = {
  endPoint: string
  bindValue: string
  bindName: string
  preventInit?: boolean
  value?: any
  placeholder?: string
  tabIndex?: number
  clearable?: boolean
  filterKey?: string
  multi?: boolean
  mergeField?:any
}
