import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { ApiService } from 'src/app/services/core/api.service';
import { apiDataType } from './types';
import { Subject, Subscription } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  switchMap,
  tap,
  finalize
} from 'rxjs/operators';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'dash-dropdown',
  template: `
    <ng-select appendTo="body"
      *ngIf="serverSide"
      [multiple]="apiData.multi || false"
      [clearable]="apiData.clearable || true"
      [items]="data"
      [virtualScroll]="true"
      [bindValue]="apiData.bindValue"
      [bindLabel]="apiData.bindName"
      [loading]="loading"
      [placeholder]="apiData.placeholder || 'select...'"
      [typeahead]="dropdownInput$"
      (scrollToEnd)="init()"
      [(ngModel)]="value"
      (change)="handleValueChange($event)"

      [readonly]="readonly"
    >
    </ng-select>
    <ng-select appendTo="body"
      *ngIf="!serverSide"
      [multiple]="apiData.multi || false"
      [clearable]="apiData.clearable || true"
      [items]="data"
      [bindValue]="apiData.bindValue"
      [bindLabel]="apiData.bindName"
      [loading]="loading"
      [placeholder]="apiData.placeholder || 'select...'"
      [(ngModel)]="value"
      (change)="handleValueChange($event)"

      [readonly]="readonly"
    >
    </ng-select>
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DashDropdownComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashDropdownComponent
  implements OnInit, ControlValueAccessor, OnChanges
{
  constructor(
    private _apiService: ApiService,
    private _cd: ChangeDetectorRef
  ) {}

  @Input() apiData: apiDataType;
  @Input() serverSide: boolean = true;
  @Input() params: any;
  @Input() readonly: any;

  private get _url() {
    return this._makeURL(this.apiData.endPoint);
  }
  @Input() value: any;
  @Input() preSelectedID: number;
  @Input() formatter: (x: any) => any;
  @Output() handleChange = new EventEmitter<any>();

  data: any[] = [];
  loading: boolean = false;
  dropdownInput$ = new Subject<string>();
  private _page: number = 1;
  private _limit = 20;
  private request: Subscription;
  private _term: string = null;
  private _preventApi: boolean = false

  ngOnInit(): void {
    if (!this.apiData.preventInit) {

      this.init();
    }

    this.initSearchSub();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.handlePreselected(changes['preSelectedID']);
  }

  private handlePreselected(changes) {
    if (!changes) return;
    if (changes) {
      let checkPrev = changes.previousValue != changes.currentValue;
      if (checkPrev && changes.currentValue) {
        if (this.request && !this._preventApi) {
          this.request.unsubscribe();
        this.init('empty');
        }
      }
    }
  }

  getData() {
    this._page = 1;
    this.init('empty');
  }

  writeValue(value: any): void {
    this.value = value;
  }

  registerOnChange(val: any) {
  }

  registerOnTouched(val: any) {
  }

  private _makeURL(url) {
    if (!this.params) return url;
    let ep: string = url;
    for (let i of Object.keys(this.params)) {
      if (!this.params[i]) continue;
      let re = new RegExp(`\\b${i}\\b`, 'gi');
      let r = ep.replace(re, this.params[i]);
      ep = r;
    }
    let sanitizedURL = ep.replace(/:/g, '');
    return sanitizedURL;
  }

  private _validURL(url: string) {
    if (!url) return false;
    return !url.includes(':');
  }

  private _detectChange() {
    this._cd.detectChanges();
  }

  private _formatFilter(filter: string | null) {
    let qp: {
      filters: string | boolean;
      limit: number;
      page: number;
      preSelectedID?: number;
    } = {
      filters: false,
      limit: this._limit,
      page: this._page,
    };
    if (filter) {
      qp.filters = JSON.stringify({
        [this.apiData.filterKey || this.apiData.bindName]: filter,
      });
    }
    if (this.preSelectedID && this._page == 1) {
      qp.preSelectedID = this.preSelectedID;
    }

    return qp;
  }

  private _stopLoading() {
    this.loading = false;
    this._detectChange();
  }

  private _reformData(arr: any[]) {
    if (!this.formatter) return arr;
    if (!this.apiData.filterKey) {
      this.apiData.filterKey = this.apiData.bindName;
    }
    this.apiData.bindName = 'formatted';

    return arr.map((x) => {
      return {
        ...x,
        formatted: this.formatter(x),
      };
    });
  }

  init(empty = null) {
    if (!this._validURL(this._url)) return;
    this.loading = true;

    this.request = this._apiService
      .get(this._url, this._formatFilter(this._term))
      .pipe(
        map((res) => this._reformData(res.data)),
        finalize(() => this._stopLoading())
        )
      .subscribe({
        next: async (data) => {
          if (empty) {
            this.data = data;
          } else {
            this._page++;
            this.data = [...this.data, ...data];
          }

          this._detectChange();
        },
        error: (err) => console.log(err),
      })
  }

  private initSearchSub() {
    if (!this._validURL(this._url)) return;
    this.dropdownInput$
      .pipe(
        distinctUntilChanged(),
        debounceTime(400),
        switchMap((term: any) => {
          this._term = term;
          this._page = 1;
          this.loading = true;
          return this._apiService
            .get(this._url, { ...this._formatFilter(term), page: 1 })
            .pipe(
              map((res) => this._reformData(res.data)),
              finalize(() => this._stopLoading())
              );
        })
      )
      .subscribe({
        next: (data: any) => {
          this.data = data;
          this._detectChange();
        },
      });
  }

  handleValueChange(e: any) {
    this._preventApi = true
    this.handleChange.emit(e);
  }
}
