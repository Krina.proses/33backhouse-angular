import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { DashDropdownComponent } from './dropdown.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [DashDropdownComponent],
  imports: [ NgSelectModule, CommonModule, FormsModule],
  exports: [DashDropdownComponent],
})
export class DropdownModule {}
