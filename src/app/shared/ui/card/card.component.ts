import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div class="card">
      <div class="card-header" *ngIf="header">{{ header }}</div>
      <div class="card-body">
        <ng-content></ng-content>
      </div>
      <div class="card-footer" *ngIf="footer">{{ footer }}</div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./card.component.scss'],
})
export class CardComponent {
  @Input('cardHeader') header: string;
  @Input('cardFooter') footer: string;
}
