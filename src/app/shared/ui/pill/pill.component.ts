import {
  ChangeDetectionStrategy,
  Component,
  Input,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-pill',
  template: `<button disabled class="mx-1 badge-{{ value }}">
    {{ text }}
  </button>`,
  styleUrls: ['./pill.component.scss'],
})
export class PillComponent {
  @Input() class: string = ''; // if want to pass ectra property
  // @Input() color:
  //   | 'default'
  //   | 'primary'
  //   | 'secondary'
  //   | 'success'
  //   | 'info'
  //   | 'warning'
  //   | 'danger'
  //   | 'light'
  //   | 'dark' = 'primary'; // button color
  @Input() size: 'large' | 'small' = 'small';
  @Input() value: boolean = false; // text of the button
  @Input() text: string | null = ''; // text of the button

  ngOnChanges(changes: SimpleChanges): void {
    // if (changes['value']) {
    //   this.text = this.value ? 'Active' : 'In Active';
    // }
  }
}
