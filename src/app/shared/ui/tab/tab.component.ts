import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss'],
})
export class TabComponent {
  @Input('tabTitle') title: string;
  @Input() active: boolean = false;
  @Input() id: string = '';
  @Input() isDisabled: boolean = false;
}
