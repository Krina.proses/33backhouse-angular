import {
  AfterContentInit,
  Component,
  ContentChildren,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  QueryList,
  SimpleChange,
  SimpleChanges,
} from '@angular/core';
import { TabComponent } from '../tab/tab.component';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements OnChanges, AfterContentInit {
  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;
  @Input() alignment?: 'verticle' | 'horizontal' = 'horizontal';
  @Input() id?: string;
  @Input() activeTab: string;
  @Output() handleChange = new EventEmitter<any>();
  @Input() tabClass = 'tabs'


  public finalClass = 'nav-link';

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes['activeTab'].currentValue &&
      !changes['activeTab'].firstChange
    ) {
      this.tabs.toArray().forEach((tab) => {
        if (tab.id == changes['activeTab'].currentValue) {
          tab.active = true;
          this.finalClass = 'nav-link active';
        } else {
          tab.active = false;
        }
      });
    }
  }

  // contentChildren are set
  ngAfterContentInit() {
    this.tabs.toArray().forEach((tab) => {
      if (tab.id == this.activeTab) {
        tab.active = true;
        this.finalClass = 'nav-link active';
      } else {
        tab.active = false;
      }
    });
  }

  selectTab(tab: TabComponent) {
    if (this.handleChange) {
      // this.handleChange.emit(tab);
      // return;
    }
    // deactivate all tabs
    this.tabs.toArray().forEach((tab) => (tab.active = false));

    // activate the tab the user has clicked on.
    tab.active = true;

    // set the class for contenct
    this.finalClass = 'nav-link active';
  }
}

// --------------------------------example-----------------------------
//         <app-card id="myTabs">
//           <app-tabs alignment="verticle">
//             <app-tab tabTitle="Tab 1">
//               Tab 1 content
//             </app-tab>
//             <app-tab tabTitle="Tab 2" [isDisabled]="true">
//               Tab 2 content
//             </app-tab>
//           </app-tabs>
//         </app-card>
