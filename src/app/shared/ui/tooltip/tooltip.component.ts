import {
  Component,
  HostListener,
  Input,
  Renderer2,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-tooltip',
  template: `
    <!-- <span data-bs-toggle="tooltip" [attr.data-bs-placement]="placement" title="{{ title }}">
    <ng-content></ng-content>
  </span> -->

    <div data-bs-toggle="tooltip" title="{{ tiptitle }}">
      <ng-content></ng-content>
    </div>
    <script>
      $(function () {
        $('[data-bs-toggle="tooltip"]').tooltip({
          placement: {{ this.placement }},
        });
      });
    </script>
    <!-- <span
      class="d-inline-block"
      tabindex="0"
      data-bs-toggle="tooltip"
      [attr.data-bs-placement]="placement"
      title="{{ title }}"
      container="body"
    >
      <ng-content></ng-content>
    </span> -->
  `,
  // templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss'],
})
export class TooltipComponent {
  @Input() tiptitle: string;
  @Input() placement: 'auto' | 'top' | 'bottom' | 'left' | 'right' = 'auto';
  @Input() delay: number;

  ngOnChanges(changes: SimpleChanges): void {
    // if (changes['placement']) {
    //   tooltip.toggle();
    // }
  }

  // constructor(private _renderer: Renderer2) {}

  // @HostListener('mouseenter') onMouseEnter() {
  //   if (!this.title) {
  //     this.show();
  //   }
  // }

  // @HostListener('mouseleave') onMouseLeave() {
  //   if (this.title) {
  //     this.hide();
  //   }
  // }

  // show() {
  //   this._renderer.addClass(this.title, 'ng-tooltip-show');
  // }

  // hide() {
  //   this._renderer.removeClass(this.title, 'ng-tooltip-show');
  //   window.setTimeout(() => {
  //     this._renderer.removeChild(document.body, this.title);
  //     this.title = null;
  //   }, this.delay);
  // }
}
