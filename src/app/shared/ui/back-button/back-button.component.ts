import { Location } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'back-button',
  template: `<app-button
    class="ms-1"
    [text]=""
    [icon]="icon"
    (onClick)="handleBack()"
  ></app-button>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BackButtonComponent {
  constructor(private _location: Location) {}
  @Input() text: string = 'Back';
  @Input() icon: string = 'fa-solid fa-arrow-left';

  handleBack() {
    this._location.back()
  }

}
