import {
  Component,
  EventEmitter,
  forwardRef,
  Input,
  Output,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-switch',

  template: ` {{ label }}
    <div class="toggle-switch py-2">
      <label
        (click)="toggleChecked()"
        [ngClass]="{ checked: isChecked, disabled: disabled }"
      >
      </label>
    </div>`,
  styleUrls: ['./switch.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SwitchComponent),
      multi: true,
    },
  ],
})
export class SwitchComponent implements ControlValueAccessor {
  @Output() handleChange = new EventEmitter<boolean>(null);
  @Input() disabled: boolean;
  @Input() label: string;
  @Input()
  public set value(isChecked: boolean) {
    if (!this.disabled) {
      this.isChecked = isChecked;
      // this.onChange(isChecked);
    }
  }

  protected isChecked: boolean;
  constructor() {
    this.disabled = false;
  }

  public onChange: any = (val: boolean) => {
    this.handleChange.emit(val);
  };
  public onTouch: any = () => {};

  public writeValue(value: any): void {
    this.value = value;
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  public toggleChecked(): void {
    if (!this.disabled) {
      this.isChecked = !this.isChecked;
      this.onChange(this.isChecked);
    }
  }
}
