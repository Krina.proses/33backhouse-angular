import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirmbox.component.html',
  styleUrls: ['./confirmbox.component.scss'],
})
export class ConfirmBoxComponent {
  @Input('modelId') id: string;
  @Input('modelTitle') title: string;
  @Input('modelMsg') message: string;
  @Input() icon: string;
  @Input('imgSource') src: string;
  @Output() onClick: any = new EventEmitter<any>(); // execution prop
}
