import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'dash-box',
  template: `
    <div [class]="alignItems" [ngStyle]="{ 'min-height': minHeight + 'vh' }">
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ['./dash-box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashBoxComponent {
  @Input() alignItems:
    | 'center'
    | 'left'
    | 'right'
    | 'bottom'
    | 'top-right'
    | 'top-left'
    | 'bottom-right'
    | 'bottom-left'
    | 'top';
  @Input() minHeight:
    | '10'
    | '20'
    | '30'
    | '40'
    | '50'
    | '60'
    | '70'
    | '80'
    | '90'
    | '100' = '100';
}
