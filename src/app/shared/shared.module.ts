import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './ui/button/button.component';
import { DashMenuModule } from './ui/menu/menu.module';
import { DashDataGridModule } from './ui/dash-data-grid/data-grid.module';
import { GridShellModule } from './ui/data-grid-shell/grid-shell.module';
import { CardComponent } from './ui/card/card.component';
import { PageHeaderComponent } from './ui/page-header/page-header.component';
import { TabComponent } from './ui/tab/tab.component';
import { TabsComponent } from './ui/tabs/tabs.component';
import { SwitchComponent } from './ui/switch/switch.component';
import { PillComponent } from './ui/pill/pill.component';
import { ConfirmBoxComponent } from './ui/confirmbox/confirmbox.component';
import { BackButtonComponent } from './ui/back-button/back-button.component';
import { DropdownModule } from './ui/dropdown/dropdown.module';
import { AppModalModule } from './ui/app-modal/app-modal.module';
import { DashInputDirective } from './directives/dash-input.directive';
import { DashBoxComponent } from './ui/box/dash-box.component';
import { VarDirective } from './directives/ng-var.directive';
import { NumberOnlyDirective } from './directives/number-only.directive';






@NgModule({
  declarations: [
    ButtonComponent,
    BackButtonComponent,
    CardComponent,
    PageHeaderComponent,
    TabComponent,
    TabsComponent,
    SwitchComponent,
    PillComponent,
    ConfirmBoxComponent,
    DashInputDirective,
    VarDirective,
    NumberOnlyDirective,
    DashBoxComponent,

  ],
  imports: [CommonModule,AppModalModule],
  exports: [
    ButtonComponent,
    DashMenuModule,
    BackButtonComponent,
    DashDataGridModule,
    GridShellModule,
    PageHeaderComponent,
    CardComponent,
    TabComponent,
    TabsComponent,
    SwitchComponent,
    PillComponent,
    ConfirmBoxComponent,
    DropdownModule,
    AppModalModule,
    DashInputDirective,
    DashBoxComponent,
    VarDirective,
    NumberOnlyDirective,

  ],
  providers: [


  ]
})
export class SharedModule {}
