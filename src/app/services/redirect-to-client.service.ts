import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RedirectToClient {

  constructor() { }


  redirect(url){
    let u = `${environment.clientURL}${url}`;
    window.open(u, "_blank")
  }

}
