

import { Injectable } from '@angular/core';
import { ApiService } from './core/api.service';
import { GlobalConstants } from './core/global-constants';
import { LocalStorageService } from './core/local-storage.service';

type userDataType  = {
createdAt: string
email:string
id: number
isActive: boolean
modifyAt: string
name: string
password: string
phoneNumber: null|string
refreshToken: null|string
roleID: number
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _ls: LocalStorageService, private _apiService: ApiService) { }

  getUsers(filterData: any) {
   return this._apiService.get('user/allUserList', filterData)
  }

  getCurrentUser(): userDataType {
   return this._ls.get(GlobalConstants.lsKeys.userData)
  }

  forgotpassword(body: any) {
    return this._apiService.post('user/forgotPassword', body);
  }

  changepassword(body:any) {
    return this._apiService.post('user/changePassword', body);
  }

}
