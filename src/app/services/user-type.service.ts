import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './core/api.service';

@Injectable({
  providedIn: 'root'
})
export class UserTypeService {

  constructor(private _apiService: ApiService) { }

  get(): Observable<any> {
   return this._apiService.get(`userType/getAllUserType`)
  }

}
