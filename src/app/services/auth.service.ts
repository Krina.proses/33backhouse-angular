import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from './core/local-storage.service';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private _ls: LocalStorageService,
    private _router: Router,
  ) { }

  goToLogin() {
    let user = this._ls.getCurrentUser();
    // console.log(user,'useruser');

    if (!user) {
      this._router.navigate(['/login']);
    }


  }
}
