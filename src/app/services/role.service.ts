import { Injectable } from '@angular/core';
import { ApiService } from './core/api.service';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private _apiService: ApiService) { }

  getRolebyID(id: number) {
    return this._apiService.get(`role/getRolebyID/${id}`);
  }

  getRoleByDropdown() {
    return this._apiService.get('role/getAllActiveRole')
  }

  addRole(body: any) {
    return this._apiService.post('role/addRole', body);
  }

  updateRole(id: number, body: any) {
    return this._apiService.put(`role/updateRole/${id}`, body);
  }

}
