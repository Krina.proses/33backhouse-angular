import { Injectable } from '@angular/core';
import { GlobalConstants } from './global-constants';
import { LocalStorageService } from './local-storage.service';
@Injectable({
  providedIn: 'root',
})
export class PersistFilterService {
  filters: Object = {};
  page: number = 1;
  private _currentUrl: String | null = null;
  constructor(private _ls: LocalStorageService) {}

  //set filters
  setFilters(filters: object) {
    this._ls.set(GlobalConstants.lsKeys.filterKey, JSON.stringify(filters));

    this.filters[GlobalConstants.lsKeys.filterKey + window.location.hash] =
      filters;
  }

  //rehydrate filters
  reHydrateFilters() {
    let StorageData = localStorage || [];
    for (var i = 0; i < StorageData.length; i++) {
      let key = StorageData.keys(i) || '';
      if (
        key.substring(0, GlobalConstants.lsKeys.filterKey.length) ==
        GlobalConstants.lsKeys.filterKey
      ) {
        if (key != '') {
          let localFilters = localStorage.getItem(key);
          this.filters[key] = JSON.parse(localFilters || '{}');
        }
      }
    }
  }

  getFilters() {
    return this.filters[
      GlobalConstants.lsKeys.filterKey + window.location.hash
    ];
  }

  //remove single filter
  removeSinglePageFilter() {
    delete this.filters[
      GlobalConstants.lsKeys.filterKey + window.location.hash
    ];
    localStorage.removeItem(
      GlobalConstants.lsKeys.filterKey + window.location.hash
    );
  }

  //handle page
  setPage(page: number) {
    this.page = page;
    localStorage.setItem(GlobalConstants.lsKeys.currentPage, page.toString());
  }

  getPage() {
    return this.page;
  }

  resetPage() {
    this.setPage(1);
  }

  setPageFromLS() {
    let p: string | null = localStorage.getItem(
      GlobalConstants.lsKeys.currentPage
    );
    if (p) {
      this.page = Number(p);
      return;
    }
    this.page = 1;
  }

  setCurrentUrl(url: String) {
    if (url !== this._currentUrl) {
      (this._currentUrl = url), this.setPage(1);
    } else {
      this.setPageFromLS();
    }
  }
}
