import { Injectable } from '@angular/core';
import {
  ActivatedRoute,
  CanActivate, UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { GlobalConstants } from './global-constants';
import { LocalStorageService } from './local-storage.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private _ls: LocalStorageService, private _as: AuthService, private route: ActivatedRoute) {}

  canActivate():
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    let tokenKey = GlobalConstants.lsKeys.token;
    let tk = this._ls.get(tokenKey);



    if (!tk) {
      this._as.goToLogin()
      return false;
    }
    return true;
  }
}
