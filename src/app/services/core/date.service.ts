import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DateService {
  constructor() {}

   formatDate(date: any, format: string = "") {

    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    if (format == "m-d-y") {
      return [month, year, day].join("-");
    } else if (format == "d-m-y") {
      return [day, month, year].join("-");
    } else if (format == "d-m") {
      return [day, month].join("-");
    } else if (format == "d/m/y") {
      return [day, month, year].join("/");
    } else if (format == "y-d-m") {
      return [year, day, month].join("-");
    } else {
      return [year, month, day].join("-");
    }
  }


}
