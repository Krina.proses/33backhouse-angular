import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, map } from 'rxjs';
import { LocalStorageService } from './local-storage.service';
import { Router } from '@angular/router';
import { UtilService } from './util.service';
@Injectable({
  providedIn: 'root',
})
export class ApiService {

  public token = 'villareal_token';
  public ip = null;

  getToken() {
    return this.token;
  }
  constructor(
    private _http: HttpClient,
    private _ls: LocalStorageService,
    private router: Router,
    private _utils: UtilService
  ) {

  }

  private makeURL(endpoint: string) {
    return `${environment.apiHost}${endpoint}`;
  }

  private userInfo() {
    let [first, second, third] = this.router.url.split('/')
    let data = {
      ...this._ls.getCurrentUser(),
      url: `${second}-${third}`,
    };
    return { userData: JSON.stringify(data) };
  }

  get(endpoint: string, queryParams?: any): Observable<any> {
    return this._http.get(this.makeURL(endpoint), { params: queryParams }).pipe(map(this._utils.extractData));
  }

  delete(endpoint: string, queryParams?: any): Observable<any> {
    return this._http.delete(this.makeURL(endpoint), { params: queryParams }).pipe(map(this._utils.extractData));
  }

  post(endpoint: string, body: any): Observable<any> {
    return this._http.post(this.makeURL(endpoint), body, {
      params: this.userInfo(),

    }).pipe(map(this._utils.extractData));
  }

  put(endpoint: string, body?: any): Observable<any> {
    return this._http.put(this.makeURL(endpoint), body,
    {
      params: this.userInfo(),

    }).pipe(map(this._utils.extractData));
  }
  // .pipe(map(this._utils.extractData))
}
