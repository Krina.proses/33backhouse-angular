import {
    HttpErrorResponse,
    HttpEvent, HttpHandler, HttpInterceptor, HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, retry, throwError } from 'rxjs';
import { DateService } from './date.service';
// import { SubscriptionName } from './global-constants';
import { LocalStorageService } from './local-storage.service';
import { CustomToastrService } from './toastr.service';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private _toast: CustomToastrService,
    private _ls: LocalStorageService,
    private _Toast: CustomToastrService,
    private _dateService: DateService
    ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    //check valid subscriptions
    // let isSubscription = this._ls.get(SubscriptionName.name);


    //validate subssription before 30 days
    // let subscriptionValidity = this._ls.get(SubscriptionName?.validity);

    // if(subscriptionValidity){
    //   subscriptionValidity = subscriptionValidity ? JSON.parse(subscriptionValidity) : null;
  
    //   let dueDays = this.checkSubscriptionDue(subscriptionValidity?.endDate);
    //   if(dueDays && dueDays <= 31 && isSubscription == false){
    //     return throwError(() => this._Toast.error(` Days left:${dueDays} Your subscription will expire soon !!`));
    //   }
    // }
    
    
    
    
    // if((request.method == 'PUT' || request.method == 'POST') && isSubscription == false){
    //   return throwError(() => this._Toast.error('Your subscription has expired'));
    // }

    return next.handle(request).pipe(

        //retry api call after timeoute or any error
        // retry(1),
        //catch error in http error response
        catchError((error: HttpErrorResponse) => {
          console.log(error,"jjjjjjjjjjjjjjj");
          
            let errorMessage: any = '';
            if (error.error instanceof ErrorEvent) {
                errorMessage = `Error: ${error.error.message}`;
            } else if(error.error.err){
              if(error.error.err.name=="SequelizeForeignKeyConstraintError"){
                errorMessage = `This Field Is Already In Use`;
              }else{
                errorMessage = `Message: ${error.error.err.name}`;
              }
            }else if(error.statusText == 'Unknown Error' || error.status == 0 || error.error.includes('html') ){
                errorMessage = `Something Went Wrong`;
            } else{
                errorMessage = `Message: ${error.error}`;
            }
            return throwError(() => this._toast.error(errorMessage));
        })
    )
  }


  // checkSubscriptionDue(endDate:string){
  //   return this._dateService.dateDiffInDays(new Date(), new Date(endDate))
  // }
}
