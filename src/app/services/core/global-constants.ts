export const GlobalConstants = {
  lsKeys: {
    token: 'token',
    userData: 'user',
    menu: 'menu',
    flatMenu: 'flat_menu',
    filterKey: 'filter_key',
    currentPage: 'current_page',
    meta: 'meta',
    initPath: 'initPath',
  },
  statusData: [
    {
      value: 1,
      label: 'Active',
    },
    {
      value: 0,
      label: 'InActive',
    },
  ],


  ItemsPerPage: [10, 20, 30, 50, 100, 300],
  DefaultItemsPerPage: 20,


  ToastConfig: {
    positionClass: 'toast-top-right',
  },



  userTypes: {
    admin: 1,
  },

  roles: {
    admin: 1,
  },

  currentCountryID: 1,

  fiveMBFileSize: 5 * 1024 * 1024, //maxFileSize
  twoMBFileSize: 2 * 1024 * 1024,    //twoFileSize



  displayInfront : [
    { value: 1, label: "Yes" },
    { value: 0, label: "No" },
  ],

};
