import { Injectable } from '@angular/core';
import { GlobalConstants } from './global-constants';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  constructor() {}
  get(key: string) {
    let data = localStorage.getItem(key);
    if (data && data != 'undefined') {
      return JSON.parse(data);
    }
    return data;
  }

  set(key: string, val: any) {
    try {
      localStorage.setItem(key, JSON.stringify(val));
      return true;
    } catch (error) {
      console.error('ERROR:setting in localstorage', error);
      return false;
    }
  }

  remove(key: string) {
    localStorage.removeItem(key);
  }


    /**
   * @returns current users details
   */
  getCurrentUser() {
    let data = localStorage.getItem(GlobalConstants.lsKeys.userData);
    if (data) {
      let pd = JSON.parse(data)
    let {updatedAt, createdAt, ...rest} = pd;

      return rest;
    }
    return data;
  }

  /**
   * @returns current users usertype
   */
  getUserType():number|null {
    let user = this.getCurrentUser();
    if(user) {
        return user.userTypeID;
    }
    return null
  }



}
