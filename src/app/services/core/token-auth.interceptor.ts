import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { GlobalConstants } from './global-constants';
import { LocalStorageService } from './local-storage.service';

@Injectable()
export class TokenAuthInterceptor implements HttpInterceptor {
  constructor(private router: Router, private _ls: LocalStorageService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    let token = this._ls.get(GlobalConstants.lsKeys.token);
    // console.log(token);

    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: token,
        },
      });
    }

    return next.handle(request).pipe(
      tap(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            return event;
          } else {
            //get error else not get return statement
            return false;
          }
        },
        (err) => {
          // console.log(err, "this is error");
          if (err.status == 401) {
            for (let key in GlobalConstants.lsKeys) {
              this._ls.remove(GlobalConstants.lsKeys[key]);
            }
            this.router.navigate(['/login']);
          }
        }
      )
    );

    return next.handle(request);
  }
}
