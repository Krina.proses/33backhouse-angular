import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as CryptoJS from 'crypto-js';
import { intervalToDuration } from 'date-fns';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UtilService {
  constructor() { }

  public host = environment.apiHost;

  public userTypes = {
    admin: 1,

  };

  public categoryImage = this.host + 'static/categoryImage/';
  public attributeImage = this.host + 'static/attributeImage/';
  public itemImageFile = this.host + 'static/itemImageFile/';

  passwordRegex = '^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$';
  emailRegex = '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$'//'^[a-z0-9\\._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$';
  urlRegex = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
  numberRegex = '^[0-9]{10}$';
  onlynumber = '^[0-9]{6,}$';
  //

  checkWhiteSpace(value: string | null) {
    return value.indexOf(' ') >= 1 ? true : false;
  }

  checkFileSize(fileSize: number, fileType: 'image' | 'file'): boolean {
    let valid = true;
    if (fileType == 'image') {
      // check image size 500 KB
      if (fileSize / 1024 > 500) {
        valid = false;
      }
    }

    if (fileType == 'file') {
      // check file size 5 MB
      if (fileSize > 5242880) {
        valid = false;
      }
    }
    return valid;
  }

  formatDate = (date: any, format = '') => {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    if (format == 'm-d-y') {
      return [month, day, year].join('-');
    } else if (format == 'd-m-y') {
      return [day, month, year].join('-');
    } else if (format == 'd-m') {
      return [day, month].join('-');
    } else if (format == 'd/m/y') {
      return [day, month, year].join('/');
    } else {
      return [year, month, day].join('-');
    }
  }

  public encodeData(data: any) {
    return btoa(unescape(encodeURIComponent(JSON.stringify(data))))


  }
  public decodeDate(data) {

    return JSON.parse(atob(data));

  }
  public newDateFormate(date) {
    return new Date(date);
  }
  public mustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }


  public dateDiffrance(date1?: any, date2?: any, formate?: any) {
    const dates = intervalToDuration({
      start: date1 ? new Date(date1) : new Date(),
      end: date2 ? new Date(date2) : new Date()
    })
    let value: string
    switch (formate) {
      case 'D:H:M':
        value = `${dates.days} : ${dates.hours} : ${dates.minutes}`

        break;
      case 'H:M:S':
        value = `${dates.hours} : ${dates.minutes} : ${dates.seconds}`

        break;

      default:
        value = `${dates.hours} : ${dates.minutes} : ${dates.seconds}`

        break;

    }
    return value
  }

  public decryptDate(data: any) {
    let cryptosecret = 'villarreal[*]#piup';
    const ciphertext = CryptoJS.AES.decrypt(data, cryptosecret);
    const decryptedData = JSON.parse(ciphertext.toString(CryptoJS.enc.Utf8));
    return decryptedData
  }

  public extractData(res: any) {

    if (!res.data) return res;
    let cryptosecret = 'ms*proses';
    var bytes = CryptoJS.AES.decrypt(res.data, cryptosecret);
    var decryptedData = bytes.toString(CryptoJS.enc.Utf8);
    let obj = {
      success: res.status,
      data: JSON.parse(decryptedData),
      msg: res.msg,
    };
    console.log('res', obj)
    return obj || res || {};
  }

  public convertTZ(date, tzString) {
    const dates = new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", { timeZone: tzString }));
    return dates
  }

}
