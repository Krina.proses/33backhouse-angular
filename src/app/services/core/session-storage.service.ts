import { Injectable } from '@angular/core';
import { GlobalConstants } from './global-constants';

@Injectable({
  providedIn: 'root',
})
export class SessionStorageService {
  constructor() {}
  get(key: string) {
    let data = sessionStorage.getItem(key);
    if (data) {
      return JSON.parse(data);
    }
    return data;
  }

  set(key: string, val: any) {
    try {
      sessionStorage.setItem(key, JSON.stringify(val));
      return true;
    } catch (error) {
      console.error('ERROR:setting in sessio storage', error);
      return false;
    }
  }

  remove(key: string) {
    sessionStorage.removeItem(key);
  }

  getCurrentUser() {
    let data = sessionStorage.getItem(GlobalConstants.lsKeys.userData);
    if (data) {
      let pd = JSON.parse(data);
      let { updatedAt, createdAt, ...rest } = pd;

      return rest;
    }
    return data;
  }
}
