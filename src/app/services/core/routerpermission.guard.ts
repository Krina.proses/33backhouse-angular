import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, NavigationStart, Router, RouterEvent, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LocalStorageService } from './local-storage.service';
import { SidenavService } from './sidenav.service';
import { CustomToastrService } from './toastr.service';

@Injectable({
  providedIn: 'root'
})
export class RouterpermissionGuard implements CanActivate {
  menuList: any[] = [];
  isMatch = false

  constructor(private _router: Router, private _ss: SidenavService, private _toast: CustomToastrService,
  ) {
    this.menuList = this._ss.getPermittedMenu()
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    let link = {
      url: null
    }
    link.url = this.validateFomrUrl(state.url, false);
    let result = this.deepCheckArray(link, this.menuList);
    // console.log(result,'this is result');
    if (!result) {
      let reUrl = this.validateFomrUrl(state.url, true);
      result = this.deepCheckArray({ url: reUrl }, this.menuList);

    }
    if (!result) {

      this._toast.error('This route has no permission')
      this._router.navigate(['']);
      //   history.back()

      return false

    } else {
      return true
    }

  }

  // check permission on every route when navigate
  deepCheckArray(e, menu) {
    // loop thorugh all the menu
    for (let value of menu) {
      // check current url permission (view=true)
      if (!value.children.length && value.permission.view == true) { //&& value.link && e.url.includes(value.link)

        return this.isMatch = true
      } else if (value.children.length) { // check children permissions
        this.deepCheckArray(e, value.children)
      }
    }
    return this.isMatch
  }


  // validate if naviagated to form
  validateFomrUrl(url, isRecheck) {


    let link: any[] = url.split('/');
    if (link.includes('form')) {
      let index = link.findIndex(item => item == 'form');
      link.splice(index, 2)
      if (!isRecheck) {
        link.push(['list'])
      }
    }
    let transformedUrl = link.join('/');
    return transformedUrl
  }

}
