import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SanitizationService {
  constructor() {}

  //sanitize filter object
  // this remove null valued keys
  sanitizeFilterObject(obj) {
    try {
      let sanitizedObject: any = {};
      for (let item of Object.keys(obj)) {
        // console.log(item,"thi is ietem in sanitize");

        if (obj[item] === false || obj[item] === 0 || !!obj[item]) {
          sanitizedObject[item] = obj[item];
        }
      }
      if (!Object.keys(sanitizedObject).length) {
        return false;
      }
      return JSON.stringify(sanitizedObject);
    } catch (error) {
      return false;
    }
  }

  //sanitize filter object
  // this remove null valued keys
  sanitizeBody(obj) {
    try {
      let sanitizedObject: any = {};

      for (let key of Object.keys(obj)) {
        if (obj[key] == false || obj[key]) {
          if (obj[key] == '' || obj[key] == 'null') {
            sanitizedObject[key] = null;
            continue;
          } else {
            sanitizedObject[key] = obj[key];
          }
        }
      }

      if (!Object.keys(sanitizedObject).length) {
        return false;
      }
      return sanitizedObject;
    } catch (error) {
      return false;
    }
  }
}
