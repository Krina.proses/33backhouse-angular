import { Injectable } from '@angular/core';
import { ActiveToast, IndividualConfig, ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class CustomToastrService {
  constructor(private toastr: ToastrService) {}

  basic: Partial<IndividualConfig> = {
    timeOut: 5000,
    extendedTimeOut: 1000,
    closeButton: true,
    positionClass: 'toast-top-center',
    progressBar: true,
    progressAnimation: 'decreasing',
    tapToDismiss: true,
  };

  show(
    message?: string,
    title?: string,
    override?: Partial<IndividualConfig>
  ): ActiveToast<any> {
    override = {
      ...this.basic,
      ...override,
    };
    return this.toastr.show(message, title, override);
  }

  success(
    message?: string,
    title?: string,
    override?: Partial<IndividualConfig>
  ): ActiveToast<any> {
    override = {
      ...this.basic,
      ...override,
    };
    return this.toastr.success(message, title, override);
  }

  warning(
    message?: string,
    title?: string,
    override?: Partial<IndividualConfig>
  ): ActiveToast<any> {
    override = {
      ...this.basic,
      ...override,
    };
    return this.toastr.warning(message, title, override);
  }

  info(
    message?: string,
    title?: string,
    override?: Partial<IndividualConfig>
  ): ActiveToast<any> {
    override = {
      ...this.basic,
      ...override,
    };
    return this.toastr.info(message, title, override);
  }

  error(
    message?: string,
    title?: string,
    override?: Partial<IndividualConfig>
  ): ActiveToast<any> {
    override = {
      ...this.basic,
      ...override,
    };
    if (typeof message == 'object') {
      //@ts-ignore
      message = message?.msg?.errors[0].msg;
    }
    let msg = message || 'something went wrong !'
    return this.toastr.error(message, title, override);
  }
}
