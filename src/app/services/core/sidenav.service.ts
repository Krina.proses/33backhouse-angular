import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { GlobalConstants } from 'src/app/services/core/global-constants';
import { LocalStorageService } from 'src/app/services/core/local-storage.service';

@Injectable({
  providedIn: 'root',
})
export class SidenavService {
  constructor(private _ls: LocalStorageService) {}

  private _sidebarStatus = new BehaviorSubject<boolean>(false);

  sidebarClosed$ = this._sidebarStatus.asObservable();

  //get menu from ls stored at the time of login.
  getPermittedMenu() {
    return this._ls.get(GlobalConstants.lsKeys.menu);
  }

  getFlatMenu() {
    return this._ls.get(GlobalConstants.lsKeys.flatMenu);
  }

  toggleSidebar() {
    this._sidebarStatus.next(!this._sidebarStatus.value);
  }
}
