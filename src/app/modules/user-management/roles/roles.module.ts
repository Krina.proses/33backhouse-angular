import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolesRoutingModule } from './roles-routing.module';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [FormComponent, ListComponent],
  imports: [
    CommonModule,
    RolesRoutingModule,
    NgSelectModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class RolesModule {}
