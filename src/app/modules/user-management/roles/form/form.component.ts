import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/core/api.service';
import { CustomToastrService } from 'src/app/services/core/toastr.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  public roleFromData: FormGroup;
  public Id: number;
  public showLoader = false;
  public submitted = false;
  public isView: boolean = false;
  public isEdit: boolean = false;
  public Roles: any = [];

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _apiService: ApiService,
    private _toast: CustomToastrService
  ) {
    // get id from ActivatedRoute params
    this._route.params.subscribe((data: any) => {
      this.Id = data.id;
    });

    this.roleFromData = this._fb.group({
      roleName: [null, Validators.required],
      isActive: [true],
    });

    // for diff mode is on
    this._route.queryParams.subscribe((data) => {
      if (data['isView']) {
        this.isView = data['isView'];
        this.roleFromData.disable();
      } else if (data['isEdit']) {
        this.isEdit = data['isEdit'];
      }
    });
  }

  get f(): any {
    return this.roleFromData.controls;
  }

  goToList() {
    history.back();
  }

  ngOnInit(): void {
    if (this.Id) {
      this.getDatabyId();
    }
  }

  getDatabyId() {
    this._apiService.get(`role/getRolebyID/${this.Id}`).subscribe(
    {
      next: (data) => {
        this.roleFromData.patchValue({
          roleName: data.data.roleName,
          isActive: (data.data.isActive)
        });
      },
      error: (err) => {
        this.showLoader = false;
        this._toast.error(null, err.error);
      }
    }
    );
  }

  onSubmit(e) {
    this.submitted = true;

    if (this.roleFromData.invalid) {
      return console.log('invalid');
    } else {
      if (!this.isEdit) {
        this.showLoader = true;
        this._apiService
          .post(`role/addRole`, this.roleFromData.value)
          .subscribe(
            {
            next: (data) => {
              this.showLoader = false;
              this._router.navigateByUrl('/user-management/roles/list');
              this._toast.success(data.msg);
            },
            error: (err) => {
              this.showLoader = false;
              this._toast.error(null, err.error);

            }}
          );
      } else {
        this.showLoader = true;

        this._apiService
          .put(`role/updateRole/${this.Id}`, {
            ...this.roleFromData.value,
            id: this.Id,
          })
          .subscribe(
           {
            next: (data) => {
              this.showLoader = false;
              this._router.navigateByUrl('/user-management/roles/list');
              this._toast.success(data.msg);
            },
            error: (err) => {
              this.showLoader = false;
              this._toast.error(null, err.error);
            }
           }
          );
      }
    }
  }
}
