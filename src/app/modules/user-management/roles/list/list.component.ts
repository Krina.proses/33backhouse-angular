import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalConstants } from 'src/app/services/core/global-constants';
import {
  actionButtonType,
  headerObject,
} from 'src/app/shared/ui/dash-data-grid/data-grid.type';
import { ShellConfig } from 'src/app/shared/ui/data-grid-shell/grid-shell.types';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  //Action buttons
  readonly actionBtn: actionButtonType[] = [
    {
      tooltip: 'view',
      class: 'far fa-eye',
      show: true,
    },
    {
      tooltip: 'edit',
      class: 'far fa-pen-to-square',
      show: true,
    },
    {
      tooltip: 'delete',
      class: 'far fa-trash-alt',
      show: true,
    },
  ];

  // Grid headers
  readonly header: headerObject[] = [
    {
      title: 'Role',
      name: 'roleName',
      visible: true,
      isSortable: true,
      filter: {
        type: 'text',
        field: 'roleName',
        bindValue: 'roleName',
      },
    },
    {
      title: 'Status',
      name: 'isActive',
      isSortable: true,
      formatter: this.statusFormatter,
      dataClass: this.statusClass,
      visible: true,
      filter: {
        type: 'select',
        bindValue: 'value',
        bindLabel: 'label',
        dataKey: 'status',
        placeholder: 'status',
      },
    },
  ];

  filterDropdownData = { status: GlobalConstants.statusData };

  constructor(private _router: Router) {
    /**
     * get permission while redirect from sidebar link
     */
    let url = _router.routerState.snapshot.url;
    // let btnPermission: any = this.permissionService.getMenuID(url);
    // // console.log(btnPermission, 'from refresh');
    // if (btnPermission.add) {
    //   this.addBtn = true;
    // }
  } // constructor ends here

  shellConfig: ShellConfig = null;

  ngOnInit(): void {
    this._initShellConfig();
  }

  // call for list data
  private _initShellConfig() {
    this.shellConfig = {
      headers: this.header,
      endpoint: 'role/getAllRoles',
    };
  }

  statusFormatter(item) {
    if (item.isActive) {
      return 'Active';
    }
    return 'In Active';
  }

  statusClass(item) {
    if (item.isActive) {
      return 'Active';
    }
    return 'InActive';
  }

  goToForm() {
    this._router.navigateByUrl('user-management/roles/form');
  }

  // click in any action in grid list
  actionButtonClick(action: any) {
    if (action.action == 'edit') {
      this._router.navigate(['user-management/roles/form/' + action.data.id], {
        queryParams: { isEdit: true },
      });
    } else if (action.action == 'view') {
      this._router.navigate(['user-management/roles/form/' + action.data.id], {
        queryParams: { isView: true },
      });
    }
  }
}
