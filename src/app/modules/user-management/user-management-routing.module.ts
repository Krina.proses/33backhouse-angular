import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../services/core/auth.guard';
import { RouterpermissionGuard } from '../../services/core/routerpermission.guard';
const routes: Routes = [

      {
        path: 'permission',
        loadChildren: () =>
          import('./permission/permission.module').then(
            (m) => m.PermissionModule
          ),
        canActivate: [AuthGuard,RouterpermissionGuard],
      },
      {
        path: 'user',
        loadChildren: () =>
          import('./users/users.module').then(
            (m) => m.UsersModule
          ),
        canActivate: [AuthGuard, RouterpermissionGuard],
      },
      {
        path: 'roles',
        loadChildren: () =>
          import('./roles/roles.module').then(
            (m) => m.RolesModule
          ),
        canActivate: [AuthGuard, RouterpermissionGuard],
      },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserManagementRoutingModule {}
