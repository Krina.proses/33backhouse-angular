import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/core/api.service';
import { CustomToastrService } from 'src/app/services/core/toastr.service';
import { UtilService } from 'src/app/services/core/util.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  public userFromData: FormGroup;
  public Id: number;
  public showLoader = false;
  public submitted = false;
  public isView: boolean = false;
  public isEdit: boolean = false;
  public Roles: any = [];
  public isWhitespace: boolean = false;
  public showPasswordMatch: boolean = false;
  public passwordType: 'text' | 'password' = 'password';
  public eyeClass: string = 'fa-solid fa-eye-slash';
  public salutation = [
    { id: "Dr.", name: "Dr." },
    { id: "Mr.", name: "Mr." },
    { id: "Mrs.", name: "Mrs." },
    { id: "Miss", name: "Miss" },
  ]
  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _apiService: ApiService,
    private _toast: CustomToastrService,
    private _util: UtilService
  ) {
    // get id from ActivatedRoute params
    this._route.params.subscribe((data: any) => {
      this.Id = data.id;
    });

    this.userFromData = this._fb.group({
      email: [null, [Validators.required, Validators.pattern(this._util.emailRegex)]],
      roleId: [null, Validators.required],
      name: [null, Validators.required],
      mobile: [null, Validators.required],
      address: [null, Validators.required],
      pincode: [null, Validators.required],
      password: [null],
      confirm_password: [null],
      isActive: [true],
      isPassword: [true]
    });

    // for diff mode is on
    this._route.queryParams.subscribe((data) => {
      if (data['isView']) {
        this.isView = data['isView'];
        this.userFromData.disable();
      } else if (data['isEdit']) {
        this.isEdit = data['isEdit'];
        // this.userFromData.enable();
      } else {
        this.userFromData.controls['password']?.setValidators(Validators.required);
        this.userFromData.controls['password']?.updateValueAndValidity();
        this.userFromData.controls['confirm_password']?.setValidators(Validators.required);
        this.userFromData.controls['confirm_password']?.updateValueAndValidity();
      }
    });
  }

  get f(): any {
    return this.userFromData.controls;
  }

  goToList() {
    history.back();
  }

  togglePassword() {
    this.passwordType = this.passwordType == 'text' ? 'password' : 'text';
  }

  passwordMatch(e: any) {
    if (e.target?.value) {
      if (e && this.userFromData.get('password')!.value !== e.target.value) {
        this.showPasswordMatch = true;
      } else {
        this.showPasswordMatch = false;
      }
    }
  }

  ngOnInit(): void {
    this.getRole();
    if (this.Id) {
      this.getDatabyId();
    }
  }

  getRole() {
    this._apiService.get(`role/getAllActiveRole`).subscribe(
      {
        next: (data) => {
          this.Roles = data.data;
        },
        error: (err) => {
          this.showLoader = false;
          this._toast.error(null, err.error);
        }
      }
    );
  }

  setRole(e: any) {
    if (e?.id) {
      this.userFromData.patchValue({ roleId: e.id });
    }
  }

  getDatabyId() {
    this._apiService.get(`user/userByID/${this.Id}`).subscribe(
      {
        next: (data) => {
          this.userFromData.patchValue({
            name: data.data.name,
            mobile: data.data.mobile,
            address: data.data.address,
            pincode: data.data.pincode,
            email: data.data.email,
            roleId: data.data.roleId,
            isActive: data.data.isActive,
            isPassword: false
          });
        },
        error: (err) => {
          this.showLoader = false;
          this._toast.error(null, err.error);
        }
      }
    );
  }

  onSubmit(e) {
    this.submitted = true;
    console.log('this.userFromData.value', this.userFromData.value);

    if (this.userFromData.invalid) {
      return console.log('invalid');
    }

    if (!this.isEdit) {
      this.showLoader = true;

      this._apiService
        .post(`user/addUser`, this.userFromData.value)
        .subscribe(
          {
            next: (data) => {
              this.showLoader = false;
              this._router.navigateByUrl('user-management/user/list');
              this._toast.success(data.msg);
            },
            error: (err) => {
              this.showLoader = false;
              this._toast.error(null, err.error);
            }
          }
        );
    } else {
      this.showLoader = true;
      if(!this.userFromData.value.isPassword){
        delete this.userFromData.value.password
        delete this.userFromData.value.confirm_password

      }
      console.log(this.userFromData.value)
      this._apiService
        .put(`user/updateUser`, {
          ...this.userFromData.value,
          id: this.Id,
        })
        .subscribe(
          {
            next: (data) => {
              this.showLoader = false;
              this._router.navigateByUrl('user-management/user/list');
              this._toast.success(data.msg);
            },
            error: (err) => {
              this.showLoader = false;
              this._toast.error(null, err.error);
            }
          }
        );
    }
  }
}
