import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DateService } from 'src/app/services/core/date.service';
import { GlobalConstants } from 'src/app/services/core/global-constants';
import {
  actionButtonType,
  headerObject,
} from 'src/app/shared/ui/dash-data-grid/data-grid.type';
import { ShellConfig } from 'src/app/shared/ui/data-grid-shell/grid-shell.types';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  //Action buttons
  readonly actionBtn: actionButtonType[] = [
    {
      tooltip: 'view',
      class: 'far fa-eye',
      show: true,
    },
    {
      tooltip: 'edit',
      class: 'far fa-pen-to-square',
      show: true,
    },
  ];

  // Grid headers
  readonly header: headerObject[] = [
    {
      title: 'Email',
      name: 'email',
      isSortable: true,
      visible: true,
      filter: {
        type: 'text',
        bindValue: 'email',
      },
    },
    {
      title: 'Role',
      name: 'role.roleName',
      isSortable: true,
      visible: true,
      filter: { type: 'text' },
    },
    {
      title: 'Status',
      name: 'isActive',
      isSortable: true,
      formatter: this.statusFormatter,
      dataClass: this.statusClass,
      visible: true,
      filter: {
        type: 'select',
        bindValue: 'value',
        bindLabel: 'label',
        dataKey: 'status',
        placeholder: 'status',
      },
    },
  ];

  filterDropdownData = { status: GlobalConstants.statusData };

  constructor(private _router: Router, private _date: DateService) {} // constructor ends here

  shellConfig: ShellConfig = null;

  ngOnInit(): void {
    this._initShellConfig();
  }

  // call for list data
  private _initShellConfig() {
    this.shellConfig = {
      headers: this.header,
      endpoint: 'user/getAllUser',
    };
  }

  statusFormatter(item) {
    if (item.isActive) {
      return 'Active';
    }
    return 'In Active';
  }

  statusClass(item) {
    if (item.isActive) {
      return 'Active';
    }
    return 'InActive';
  }

  goToForm() {
    this._router.navigateByUrl('user-management/user/form');
  }

  // click in any action in grid list
  actionButtonClick(action: any) {
    if (action.action == 'edit') {
      this._router.navigate(['user-management/user/form/' + action.data.id], {
        queryParams: { isEdit: true },
      });
    } else if (action.action == 'view') {
      this._router.navigate(['user-management/user/form/' + action.data.id], {
        queryParams: { isView: true },
      });
    }
  }
}
