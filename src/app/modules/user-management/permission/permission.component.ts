import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/core/api.service';
import { CustomToastrService } from 'src/app/services/core/toastr.service';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent implements OnInit {
  roleList: any = [];
  menuList: any = [];
  finalList: any = [];
  permissionArr: any = {};
  finalObj: any = {};
  role_id = null;
  All: any = { viewAll: false, addAll: false, editAll: false, deleteAll: false, exportAll: false, multiActionAll:false, disabled: false };
  TotalCount: number = 0;
  counter: number = 0;

  constructor(private router: Router, private _apiService: ApiService, private _toast: CustomToastrService) { }

  ngOnInit(): void {
    this.getRoles();
  }


  getRoles() {
    this._apiService.get('role/getAllActiveRole', {})
      .subscribe(
      {
        next: async (data) => {
          console.log(data, 'this is get data');

          this.roleList = data.data;
        }, error: (err) => {
          this._toast.error(`${err.error.msg}-custom`);
        }
      });
  }

  changeRole(event) {
    console.log(event);

    this.role_id = event ? event.id : null;
    if (!this.role_id) {
      return
    }
    let per = event?.permission || null;

    // this.permissionArr = per ? JSON.parse(per) : null;

    this.finalObj = per ? JSON.parse(per) : null;
    if (this.finalObj) {
      this.All = {
        viewAll: this.finalObj['viewAll'] ? this.finalObj['viewAll'] : false,
        addAll: this.finalObj['addAll'] ? this.finalObj['addAll'] : false,
        editAll: this.finalObj['editAll'] ? this.finalObj['editAll'] : false,
        deleteAll: this.finalObj['deleteAll'] ? this.finalObj['deleteAll'] : false,
        exportAll: this.finalObj['exportAll'] ? this.finalObj['exportAll'] : false,
        multiActionAll: this.finalObj['multiActionAll'] ? this.finalObj['multiActionAll'] : false,


        disabled: this.finalObj['addAll'] && this.finalObj['editAll'] && this.finalObj['deleteAll'] && this.finalObj['exportAll'] && this.finalObj['multiActionAll'] ? true : false
      }
    }

    this._apiService.get('permission/getMenuPermissionList', { id: event.id})
      .subscribe(
        {
        next: async (data) => {

          this.menuList = data.data;
          console.log(data.data, 'this is dta .dat ');

          this.getCheckAll(this.menuList);

        }, error: (err) => {
          this._toast.error(`${err.error.msg}-custom`);
        }
      });

  }

  getCheckAll(menu) {

    return menu.filter((item) => {
      /** check permssion from permission object  */
      // console.log(item,'this is item');

      if (item?.children?.length == 0) {
        console.log(item, 'these are parent');

        this.TotalCount++;
        const singlePerm = item?.permission;
        if (!singlePerm?.['view']) {
          this.finalObj = { ...this.finalObj, viewAll: false, addAll: false, editAll: false, deleteAll: false, exportAll: false,multiActionAll:false };
        }
        return true;
      }
      else if (item?.children?.length > 0) {

        /**if menu have children then recursive call for pemission set in child menu */
        item.children = this.getCheckAll(item.children);
        return item.children.length > 0;
      }
      else {
        return false;
      }
    });
  }


  // sigle checkbox clicked
  checkboxClicked(event, item, name) {
    const value = event.target.checked;


    this.setMenuPermission(this.menuList, item, value, name);

    // if(value) if(this.TotalCount == this.counter + 1) this.finalObj = { ...this.finalObj, viewAll: value }
    // if(!value) if(this.TotalCount == this.counter) this.finalObj = { ...this.finalObj, viewAll: value }

    // this.counter = 0;
  }

  // All checkbox clicked
  allCheckboxClicked(event, name) {
    const value = event.target.checked;



    this.All = { ...this.All, [`${name}All`]: value };

    let check = this.All['addAll'] || this.All['editAll'] || this.All['deleteAll'] || this.All['exportAll'] || this.All['multiActionAll'] ? true : false

    this.All = { ...this.All, disabled: check };

    this.finalObj = { ...this.finalObj, ...this.All };

    this.setMenuPermissionAll(this.menuList, value, name);

  }

  // recursive fun for All checkbox clicked
  setMenuPermissionAll(menu, value, name) {

    return menu.filter((item) => {

      let basePerm = {
        view: false,
        add: false,
        edit: false,
        delete: false,
        export: false,
        multiAction:false,
        disabled: false,
      };

      /** check permssion from permission object  */

      if (item?.children?.length == 0) {
        basePerm = item ? item.permission ? item.permission : basePerm : basePerm;

        const singlePerm = { ...basePerm, disabled: this.All['disabled'], [name]: value };
        this.finalObj = { ...this.finalObj, [item.id]: singlePerm }

        item.permission = singlePerm;
        return true;
      }
      else if (item?.children?.length > 0) {
        /**if menu have children then recursive call for pemission set in child menu */
        item.children = this.setMenuPermissionAll(item.children, value, name);
        return item.children.length > 0;
      }
      else {
        return false;
      }
    });
  }

  // recursive fun for single checkbox clicked
  setMenuPermission(menu, obj, value, name) {

    return menu.filter((item) => {

      let basePerm = {
        view: false,
        add: false,
        edit: false,
        delete: false,
        export: false,
        multiAction:false,
        disabled: false,
      };

      /** check permssion from permission assign to that menu  */

      if (item?.children?.length == 0) {

        basePerm = item ? item.permission ? item.permission : basePerm : basePerm;

        // if(basePerm['view']) this.counter++;

        if (item.id == obj.id) {
          const singlePerm = { ...basePerm, [name]: value };
          let check = (singlePerm['add'] || singlePerm['edit'] || singlePerm['delete'] || singlePerm['export'] || singlePerm['multiAction']) ? true : false;
          item.permission = { ...singlePerm, view: singlePerm['view'], disabled: check };
          console.log('item.permission', item.permission);

          this.finalObj = { ...this.finalObj, [item.id]: { ...this.finalObj[item.id], [name]: value, view: singlePerm['view'], disabled: check } };
          // console.log('this.finalObj', this.finalObj)
        } else {
          item.permission = basePerm;
        }
        return true;
      }
      else if (item?.children?.length > 0) {
        /**if menu have children then recursive call for pemission set in child menu */
        item.children = this.setMenuPermission(item.children, obj, value, name);
        return item.children.length > 0;
      }
      else {
        return false;
      }
    });
  }

  // submit the permission method for perticular role
  onSubmit() {

    if (!this.role_id) {
      this._toast.error('Role cannot be Empty-custom');
      return;
    } else if (!this.finalObj) {
      this._toast.error('Permission cannot be Empty-custom');
      return;
    }

    this._apiService.post('permission/addMenuPermission', { id: this.role_id, permission: JSON.stringify(this.finalObj) })
      .subscribe(
        {
        next: (data) => {
          this._toast.success(data.msg);
        },
        error: (err) => {
          this._toast.error(`${err.error.msg}-custom`);
        }}
        )
  }

}

