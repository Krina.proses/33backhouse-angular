import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { SharedModule } from 'src/app/shared/shared.module';
// import { PageHeaderComponent } from 'src/app/shared/ui/page-header/page-header.component';
// import { ButtonComponent } from 'src/app/shared/ui/button/button.component';


@NgModule({
  declarations: [
    DashboardComponent,
    // PageHeaderComponent,
    // ButtonComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    NgxEchartsModule,
    SharedModule
  ]
})
export class DashboardModule { }
