import { Component, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';
import { ApiService } from 'src/app/services/core/api.service';
import { CustomToastrService } from 'src/app/services/core/toastr.service';
import { GlobalConstants } from 'src/app/services/core/global-constants';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/services/core/local-storage.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {

  options: any;
  countMember: any = 10;
  countTeacher: any = 10;
  countBranch: any = 10;
  countInstitutes: any = 10;
  countStudent: any = 10;
  map: Map<string, number> = new Map<string, number>();
  constructor(private _apiService: ApiService, private _toast: CustomToastrService, private _router: Router, private _ls: LocalStorageService,

    ) {

      // let initPath = this._ls.get(GlobalConstants.lsKeys.initPath);
      // if(initPath == null){
      // this._router.navigate(['/']);

      // }
      // this._router.navigate([initPath])

  }

  ngOnInit(): void {

  }


  // initialize the echarts instance

  Products: EChartsOption = {

    // option = {
    tooltip: {},
    xAxis: [{
      type: 'category',
      data: ['Biscoff Tin Cake', 'Blueberry Panna Cotta', 'Brownie Cookie', 'Butter Cookie', 'Cashew Cookie', 'Chocolate Rush', 'Chocolate Tin Cake', 'Classic Tiramisu', 'Coconut Tin Cake'],
      // interval: 0,
    }],
    yAxis: {
      type: 'value',
      // max: 2500,
      // min: 0
    },
    series: [
      {
        data: [1200, 2000, 1500, 1800, 2200, 1200, 2000, 1500, 1800,],
        type: 'line',
        animation: true,
        smooth: true,
      }
    ]
    //  };
  };

  WeeklySales: EChartsOption = {
    tooltip: {},
    xAxis: [{
      type: 'category',
      data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
      axisLine: {
        lineStyle: {
          width: 1,
          color: 'white',
          shadowColor: 'black',
          shadowOffsetY: 2
        }
      },
      axisLabel: {
        color: 'black'
      },
      axisTick: {
        lineStyle: {
          color: 'black'
        }
      }
    }],
    yAxis: [{
      type: 'value',
      max: 5000,
      min: 0
    }],
    series: [{
      name: "Product Order",
      type: 'bar',
      data: [1100, 3400, 1200, 2300, 4300, 6400, 4500],
      barWidth: 10,
      itemStyle: {
        borderRadius: [40, 40, 40, 40]
      },
      animation: true,
    },
    {
      name: "Product Order",
      type: 'line',
      data: [1100, 3400, 1200, 2300, 4300, 6400, 4500],
      animation: true,
      smooth: true,
    },
  ]
  };
}
