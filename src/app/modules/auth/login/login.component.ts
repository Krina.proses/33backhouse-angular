import { Component, OnInit } from '@angular/core';
import {
  FormBuilder, FormGroup,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/core/api.service';
import { GlobalConstants } from 'src/app/services/core/global-constants';
import { LocalStorageService } from 'src/app/services/core/local-storage.service';
import { CustomToastrService } from 'src/app/services/core/toastr.service';
import { UtilService } from 'src/app/services/core/util.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  fieldTextType: boolean = false;
  logindata: FormGroup;
  showLoader = false;
  submitted = false;

  public FnYears: any = [];
  constructor(
    private _fb: FormBuilder,
    private _apiService: ApiService,
    private _router: Router,
    private _ls: LocalStorageService,
    private _toast: CustomToastrService,
    private _util: UtilService,
  ) { }

  ngOnInit(): void {
    let tk = this._ls.get(GlobalConstants.lsKeys.token);
    let initPath = this._ls.get(GlobalConstants.lsKeys.initPath);

    if (tk) {
      if(initPath == null){
      this._router.navigate(['/']);

      }else{
        this._router.navigate([initPath])

      }
    }

    this.logindata = this._fb.group({
      email: [
        '',
        [Validators.required, Validators.pattern(this._util.emailRegex)],
      ],
      password: ['', Validators.required],
    });
  }

  get f(): any {
    return this.logindata.controls;
  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }


  goback() {
    this._router.navigate(['/forgot-password']);
  }

  onSubmit() {
    if (this.logindata.invalid) {
      console.log('invalid');
      return;
    } else {
      this.showLoader = true;

      this._apiService.post('user/login', this.logindata.value).subscribe(
        {
        next: (data) => {

          this.showLoader = false;
          this.submitted = false;

          const { token, user, menu, flatMenu, meta, initPath, vidoePay } = data.data
          this._ls.set(GlobalConstants.lsKeys.token, token);
          this._ls.set(GlobalConstants.lsKeys.userData, user);
          this._ls.set(GlobalConstants.lsKeys.menu, menu);
          this._ls.set(GlobalConstants.lsKeys.flatMenu, flatMenu);
          this._ls.set(GlobalConstants.lsKeys.meta, meta);
          this._ls.set(GlobalConstants.lsKeys.initPath, initPath);


            this._router.navigate(['/']);

        },
        error: (err) => {
          this.showLoader = false;
          this._toast.error(null, err.error);
        }
      }
      );
    }
  }

}
