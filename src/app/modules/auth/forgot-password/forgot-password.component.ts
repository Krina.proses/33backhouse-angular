import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CustomToastrService } from 'src/app/services/core/toastr.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {
  forgotPwdData: FormGroup;
  showLoader = false;
  submitted = false;
  company: { name: string; logo: string } = { name: '', logo: '' };
  public FnYears: any = [];
  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private _toast: CustomToastrService,
    private _userService: UserService,
  ) {}

  ngOnInit(): void {
    this.forgotPwdData = this._fb.group({
      email: [
        null,
        [
          Validators.required,
          Validators.pattern('^[a-z0-9\\._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
        ],
      ],
    });
  }

  get ff(): any {
    return this.forgotPwdData.controls;
  }

  back() {
    this._router.navigate(['/login']);
  }

  onSend() {
    this.submitted = true;

    if (this.forgotPwdData.invalid) {
      return;
    } else {
      this.showLoader = true;
      this._userService.forgotpassword(this.forgotPwdData.value)
        .subscribe(
          {
          next: (data) => {
            this.submitted = false;
            this.showLoader = false;
            this._toast.success(data.msg, 'Password Reset');
            this._router.navigate(['/login']);
          },
          error: (err) => {
            this._toast.error(null, err.error);
            this.showLoader = false;
          }
          }
        );
    }
  }
}
