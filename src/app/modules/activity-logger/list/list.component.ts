import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalConstants } from 'src/app/services/core/global-constants';
import {
  actionButtonType,
  headerObject,
} from 'src/app/shared/ui/dash-data-grid/data-grid.type';
import { ShellConfig } from 'src/app/shared/ui/data-grid-shell/grid-shell.types';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {

  // Grid headers
  readonly header: headerObject[] = [
    {
      title: 'User Email',
      name: 'User.email',
      visible: true,
      isSortable: true,
      filter: {
        type: 'number',
        field: 'User.email',
        bindValue: 'User.email',
      },
    },
    {
      title: 'oldData',
      name: 'oldData',
      visible: true,
    },
    {
      title: 'newData',
      name: 'newData',
      visible: true
    },
    {
      title: 'Type',
      name: 'activityType',
      visible: true,
      isSortable: true,
      filter: {
        type: 'text',
        field: 'activityType',
        bindValue: 'activityType'
      }
    }
  ];

  filterDropdownData = { status: GlobalConstants.statusData };

  constructor(private _router: Router) {
    /**
     * get permission while redirect from sidebar link
     */
    let url = _router.routerState.snapshot.url;
    // let btnPermission: any = this.permissionService.getMenuID(url);
    // // console.log(btnPermission, 'from refresh');
    // if (btnPermission.add) {
    //   this.addBtn = true;
    // }
  } // constructor ends here

  shellConfig: ShellConfig = null;

  ngOnInit(): void {
    this._initShellConfig();
  }

  // call for list data
  private _initShellConfig() {
    this.shellConfig = {
      headers: this.header,
      endpoint: 'activitylogs/getActivityLogsFilterWise',
    };
  }


  // statusFormatter(item) {
  //   if (item.isActive) {
  //     return 'Active';
  //   }
  //   return 'In Active';
  // }

  // statusClass(item) {
  //   if (item.isActive) {
  //     return 'Active';
  //   }
  //   return 'InActive';
  // }

  // // click in any action in grid list
  // actionButtonClick(action: any) {
  //   if (action.action == 'edit') {
  //     this._router.navigate(['user-management/roles/form/' + action.data.id], {
  //       queryParams: { isEdit: true },
  //     });
  //   } else if (action.action == 'view') {
  //     this._router.navigate(['user-management/roles/form/' + action.data.id], {
  //       queryParams: { isView: true },
  //     });
  //   }
  // }
}
