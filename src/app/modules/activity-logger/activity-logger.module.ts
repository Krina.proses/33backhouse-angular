import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivityLoggerRoutingModule } from './activity-logger-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ListComponent } from './list/list.component';

@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    ActivityLoggerRoutingModule,
    NgSelectModule,
    SharedModule,
    ReactiveFormsModule,
  ]
})
export class ActivityLoggerModule { }
