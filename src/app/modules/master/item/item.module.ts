import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemRoutingModule } from './item-routing.module';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ItemImagesComponent } from './item-images/item-images.component';
import { QuillModule } from 'ngx-quill';


@NgModule({
  declarations: [
    FormComponent,
    ListComponent,
    ItemImagesComponent
  ],
  imports: [
    CommonModule,
    ItemRoutingModule,
    NgSelectModule,
    SharedModule,
    ReactiveFormsModule,
    QuillModule.forRoot(),
  ]
})
export class ItemModule { }
