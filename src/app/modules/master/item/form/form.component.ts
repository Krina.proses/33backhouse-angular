import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/core/api.service';
import { CustomToastrService } from 'src/app/services/core/toastr.service';
import { UtilService } from 'src/app/services/core/util.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  public itemForm: FormGroup;
  public Id: number;
  public showLoader = false;
  public submitted = false;
  public isView: boolean = false;
  public isEdit: boolean = false;
  public title: string = 'Add';
  public activeTab: any = "ItemDetail"
  categoryList: any;
  attributeList: any;
  attriId: any
  attributeDetailList: any
  itemData: any


  modules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],

      [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered' }, { 'list': 'bullet' }],
      [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
      [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
      [{ 'direction': 'rtl' }],                         // text direction

      [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'font': [] }],
      [{ 'align': [] }],

      ['clean'],                                         // remove formatting button

      // ['link', 'image', 'video'],
    ],
  };

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _apiService: ApiService,
    private _toast: CustomToastrService,
    private _util: UtilService
  ) {
    this._route.params.subscribe((data: any) => {
      this.Id = data.id;
    });

    this._route.queryParams.subscribe((data) => {
      if (data['isView']) {
        this.title = 'View';
        this.isView = data['isView'];
      } else if (data['isEdit']) {
        this.title = 'Edit';
        this.isEdit = data['isEdit'];
      } else if (!data['Id']) {
      }
    });
  }

  ngOnInit(): void {
    this.formBuild();
    this.getAllCategory();
    // this.getAllAttribute();
    if (this.Id) {
      this.getDatabyId();
      // this.getAllAttribute();
    }
  }

  formBuild() {
    this.itemForm = this._fb.group({
      itemName: [null, [Validators.required]],
      categoryId: [null, [Validators.required]],
      basicPrice: [null, [Validators.required]],
      description: [null],
      // attributeDetail: this._fb.array([]),
      isActive: [true],
    });
    // this.addattributeRow();
  }

  // get attributeDetail() {
  //   return this.itemForm.get('attributeDetail') as FormArray;
  // }

  // setattributeDetail(list: any[]) {
  //   if (list.length !== 0) {
  //     const fg = list.map((i) => this._fb.group(i));
  //     const fa = this._fb.array(fg);

  //     this.itemForm.setControl('attributeDetail', fa);
  //   }
  // }

  // patchattributeDetailList(list: any[]) {
  //   this.itemForm.patchValue({
  //     attributeDetail: list,
  //   });
  // }

  // addattributeRow() {
  //   const obj = this._fb.group({
  //     attributeId: [null, [Validators.required]],
  //     attributeDetailId: [null],
  //     price: [null],
  //     isActive: [true],
  //   });

  //   this.attributeDetail.push(obj);
  // }

  // removeattributeRow(i: number) {
  //   this.attributeDetail.removeAt(i);
  // }

  get f(): any {
    return this.itemForm.controls;
  }

  goToList() {
    history.back();
  }

  getAllCategory() {
    this._apiService.get(`category/getCategoryDropDown`).subscribe({
      next: (data) => {        
        this.categoryList = data.data;
      },

      error: (err) => {
        this.showLoader = false;
      },
    });
  }

  // getAllAttribute() {
  //   this._apiService.get(`attribute/getActiveAttribute`).subscribe({
  //     next: (data) => {        
  //       this.attributeList = data.data;
  //     },

  //     error: (err) => {
  //       this.showLoader = false;
  //     },
  //   });
  // }

  // setAttribute(e: any) {
  //   this.getAllAttributeDetailById(e.id);
  //   this.itemForm.patchValue({
  //     attributeDetailId: null
  //   });
  // }

  // getAllAttributeDetailById(id: any) {
  //   this._apiService.get(`attribute/getAttrDetbyAttributeId/${id}`).subscribe({
  //     next: (data) => {        
  //       this.attributeDetailList = data.data;
  //     },

  //     error: (err) => {
  //       this.showLoader = false;
  //     },
  //   });
  // }

  getDatabyId() {
    this._apiService.get(`item/getItemByID/${this.Id}`).subscribe({
      next: (data) => {
        
        this.itemData = data.data;
        this.itemForm.patchValue({
          itemName: data.data.itemName,
          categoryId: data.data.categoryId,
          basicPrice: data.data.basicPrice,
          description: data.data.description,
          attributeDetail: data.data.ItemDetails,
          isActive: data.data.isActive,
        });
        // this.setattributeDetail(data.data.ItemDetails || []);

        // for(let item of data.data.ItemDetails) {
        //   this.getAllAttributeDetailById(item.attributeId)
        // }

        if (this.isView) {
          this.itemForm.disable();
        }
      },

      error: (err) => {
        this.showLoader = false;
      },
    });
  }


  onSubmit(e) {
    this.submitted = true;

    if (this.itemForm.invalid) {
      return console.log('invalid');
    }

    let formValues = this.itemForm.value;

    if (!this.isEdit) {
      this.showLoader = true;

      this._apiService.post(`item/addItem`, formValues).subscribe({
        next: (data) => {
          this.showLoader = false;
          this._router.navigateByUrl('master/item');
          this._toast.success(data.msg);
        },
        error: (err) => {
          this.showLoader = false;
          this._toast.error(null, err.error);
        },
      });
    } else {
      this.showLoader = true;
      this._apiService
        .put(`item/updateItem/${this.Id}`, formValues)
        .subscribe({
          next: (data) => {
            this.showLoader = false;
            this._router.navigateByUrl('master/item');
            this._toast.success(data.msg);
          },
          error: (err) => {
            this.showLoader = false;
            this._toast.error(null, err.error);
          },
        });
    }
  }

}
