import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/core/api.service';
import { DateService } from 'src/app/services/core/date.service';
import { GlobalConstants } from 'src/app/services/core/global-constants';
import { UtilService } from 'src/app/services/core/util.service';
import {
  actionButtonType,
  headerObject,
} from 'src/app/shared/ui/dash-data-grid/data-grid.type';
import { GridShellComponent } from 'src/app/shared/ui/data-grid-shell/grid-shell.component';
import { ShellConfig } from 'src/app/shared/ui/data-grid-shell/grid-shell.types';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  @ViewChild(GridShellComponent)
  public gridShell: GridShellComponent;
  public showLoader: boolean;

  imgBaseURL = this._util.itemImageFile;


  categoryFormatter(item: any) {
    return item.Category.title;
  }

  //Action buttons
  readonly actionBtn: actionButtonType[] = [
    {
      tooltip: 'view',
      class: 'far fa-eye',
      show: true,
    },
    {
      tooltip: 'edit',
      class: 'far fa-pen-to-square',
      show: true,
    },
    {
      tooltip: 'delete',
      class: 'far fa-trash-alt',
      show: true,
    },
  ];

  // Grid headers
  readonly header: headerObject[] = [
    {
      title: 'Image',
      name: 'itemImages[0].image',
      headerClass: this.imageHeaderWidth,
      type: 'html',
      dataClass: 'logoimg',
      formatter: ((data:any)=>{return this.renderImage(data, this.imgBaseURL)}),
      visible: true,
    },
    {
      title: 'Category',
      name: 'Category.title',
      visible: true,
      isSortable: true,
      formatter: this.categoryFormatter,
      filter: {
        type: 'text',
        field: 'Category.title',
        bindValue: 'Category.title',
      },
    },
    {
      title: 'Item',
      name: 'itemName',
      visible: true,
      isSortable: true,
      filter: {
        type: 'text',
        field: 'itemName',
        bindValue: 'itemName',
      },
    },
    {
      title: 'Price',
      name: 'basicPrice',
      visible: true,
      isSortable: true,
      filter: {
        type: 'text',
        field: 'basicPrice',
        bindValue: 'basicPrice',
      },
    },
    {
      title: 'Status',
      name: 'isActive',
      isSortable: true,
      formatter: this.statusFormatter,
      dataClass: this.statusClass,
      visible: true,
      filter: {
        type: 'select',
        bindValue: 'value',
        bindLabel: 'label',
        dataKey: 'status',
        placeholder: 'Status',
      },
    },
  ];

  filterDropdownData = {
    status: GlobalConstants.statusData,
  };

  constructor(
    private _router: Router,
    private _date: DateService,
    private _apiService: ApiService,
    private _toast: ToastrService,
    private _util: UtilService,
  ) {}

  shellConfig: ShellConfig = null;

  ngOnInit(): void {
    this._initShellConfig();
  }

  // call for list data
  private _initShellConfig() {
    this.shellConfig = {
      headers: this.header,
      endpoint: 'item/getAllItemFilterWise',
    };
  }

  imageHeaderWidth(){
    return 'imageWidthCustom'
  }
  statusFormatter(item) {
    if (item.isActive) {
      return 'Active';
    }
    return 'In Active';
  }

  statusClass(item) {
    if (item.isActive) {
      return 'Active';
    }
    return 'InActive';
  }

  renderImage(data:any, url:any) {    
    if(data?.itemImages[0]?.image){
      let fullPath = url + data?.itemImages[0]?.image;
      return `<img src="${fullPath}" height=50 width=50>`;
    }
    else{
      return '-'
    }
  }

  goToForm() {
    this._router.navigateByUrl('master/item/form');
  }

  // click in any action in grid list
  actionButtonClick(action: any) {
    if (action.action == 'edit') {
      this._router.navigate(['master/item/form/' + action.data.id], {
        queryParams: { isEdit: true },
      });
    } else if (action.action == 'view') {
      this._router.navigate(['master/item/form/' + action.data.id], {
        queryParams: { isView: true },
      });
    } else if ((action.action = 'delete')) {
      Swal.fire({
        title: 'Are you sure',
        text: "You won't be able to revert this",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {
          this._apiService
            //soft delete so we are just updating it in the database
            .delete(`item/deleteItem/${action.data.id}`)
            .subscribe({
              next: (data) => {
                this.showLoader = false;
                this._toast.success(data.msg);
                this.gridShell.refresh();
                // this._router.navigateByUrl('/category/list');
              },
              error: (err) => {
                this.showLoader = false;
                this._toast.error(null, err.error.msg);
              },
            });
        }
      });
    }
  }
}
