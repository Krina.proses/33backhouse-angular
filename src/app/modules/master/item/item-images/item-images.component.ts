import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/core/api.service';
import { GlobalConstants } from 'src/app/services/core/global-constants';
import { CustomToastrService } from 'src/app/services/core/toastr.service';
import { UtilService } from 'src/app/services/core/util.service';

@Component({
  selector: 'app-item-images',
  templateUrl: './item-images.component.html',
  styleUrls: ['./item-images.component.scss'],
})
export class ItemImagesComponent implements OnInit {
  public itemImagesForm: FormGroup;
  public Id: number;
  public showLoader = false;
  public submitted = false;
  public isView: boolean = false;
  public isEdit: boolean = false;
  public title: string = 'Add';
  previewImages: any[] = [];
  images: any[] = [];
  itemImage: any;
  editImages: any[] = [];

  public imgBaseURL = this._util.itemImageFile;



  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _apiService: ApiService,
    private _toast: CustomToastrService,
    private _util: UtilService
  ) {
    this._route.params.subscribe((data: any) => {
      this.Id = data.id;
    });

    this._route.queryParams.subscribe((data) => {
      if (data['isView']) {
        this.title = 'View';
        this.isView = data['isView'];
      } else if (data['isEdit']) {
        this.title = 'Edit';
        this.isEdit = data['isEdit'];
      } else if (!data['Id']) {
      }
    });
  }

  ngOnInit(): void {
    console.log(this.Id, 'tttttttttttttttttt.Id');

    this.formBuild();
    if (this.Id) {
      this.getDatabyId();
    }
  }

  formBuild() {
    this.itemImagesForm = this._fb.group({
      image: [null],
      isActive: [true],
    });
  }

  get f(): any {
    return this.itemImagesForm.controls;
  }

  goToList() {
    history.back();
  }

  handleImageChange(event) {
    console.log(event, 'eventtttttttt');
    console.log(event.target.files, 'eventtttttttt');

    if (!event.target.files.length) {
      alert('Please select file');
      return;
    }

    let myFile = event.target.files[0];
    if (myFile.size > GlobalConstants.twoMBFileSize) {
      event.target.value = '';
      alert('Please Upload file lessthen 2MB...');
      return;
    }

    if (
      myFile.name.includes('png') ||
      myFile.name.includes('jpg') ||
      myFile.name.includes('jpeg')
    ) {
    } else {
      event.target.value = '';
      alert('Please Select Image File - .png, .jpg, .jpeg');
      return;
    }

    // this.editImage = null;
    // this.image = myFile;

    this.images.push(myFile);
    var reader = new FileReader();
    // reader.readAsDataURL(myFile);
    reader.onload = (e: any) => {
      this.previewImages.push(e.target.result);
    };
    reader.readAsDataURL(myFile);
  }

  handleImageDelete(index: number) {
    this.previewImages.splice(index, 1);
    this.images.splice(index, 1);
  }

  getDatabyId() {
    console.log(this.Id, 'this.Id');

    this._apiService.get(`item/getItemByID/${this.Id}`).subscribe({
      next: (data) => {
        console.log(data.data, 'data.data');

        this.itemImage = data.data?.itemImages;
        this.editImages = this.itemImage?.filter((x) => x.image);
        if (this.isView) {
          this.itemImagesForm.disable();
        }
      },

      error: (err) => {
        this.showLoader = false;
      },
    });
  }

  onSubmit(e) {
    this.submitted = true;

    if (this.itemImagesForm.invalid) {
      return console.log('invalid');
    }

    let formValues = this.itemImagesForm.value;

    const formData = new FormData();

    for (let item of this.images) {
      formData.append('FILES', item);
    }

    formData.append(
      'old',
      JSON.stringify({
        image: this.editImages,
      })
    );

    // if (!this.isEdit) {
    //   this.showLoader = true;

    //   this._apiService.post(`item/addItem`, formValues).subscribe({
    //     next: (data) => {
    //       this.showLoader = false;
    //       this._router.navigateByUrl('master/item');
    //       this._toast.success(data.msg);
    //     },
    //     error: (err) => {
    //       this.showLoader = false;
    //       // this._toast.error(null, err.error);
    //     },
    //   });
    // } else {
    this.showLoader = true;
    this._apiService
      .put(`item/updateItemImages/${this.Id}`, formData)
      .subscribe({
        next: (data) => {
          this.showLoader = false;
          this._router.navigateByUrl('master/item');
          this._toast.success(data.msg);
        },
        error: (err) => {
          console.log(err, 'errrorrrrrrr');

          this.showLoader = false;
          this._toast.error(null, err.error);
        },
      });
    // }
  }
}
