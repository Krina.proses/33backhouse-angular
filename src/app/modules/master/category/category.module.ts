import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryRoutingModule } from './category-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';

import { ListComponent } from './list/list-component';
import { FormComponent } from './form/form-component';

@NgModule({
  declarations: [ListComponent, FormComponent],
  imports: [
    CommonModule,
    CategoryRoutingModule,
    NgSelectModule,
    SharedModule,
    ReactiveFormsModule,
  ],
})
export class CategoryModule {}
