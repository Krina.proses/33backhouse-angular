import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DateService } from 'src/app/services/core/date.service';
import { GlobalConstants } from 'src/app/services/core/global-constants';
import { ApiService } from 'src/app/services/core/api.service';
import Swal from 'sweetalert2';
import {
  actionButtonType,
  headerObject,
} from 'src/app/shared/ui/dash-data-grid/data-grid.type';
import { ShellConfig } from 'src/app/shared/ui/data-grid-shell/grid-shell.types';
import { ToastrService } from 'ngx-toastr';
import { GridShellComponent } from 'src/app/shared/ui/data-grid-shell/grid-shell.component';
import { UtilService } from 'src/app/services/core/util.service';

@Component({
  selector: 'app-list',
  templateUrl: './list-component.html',
  styleUrls: ['./list-component.scss'],
})
export class ListComponent implements OnInit {
  @ViewChild(GridShellComponent)
  public gridShell: GridShellComponent;
  public showLoader: boolean;
  imgBaseURL = this._util.categoryImage;


  //Action buttons
  readonly actionBtn: actionButtonType[] = [
    {
      tooltip: 'view',
      class: 'far fa-eye',
      show: true,
    },
    {
      tooltip: 'edit',
      class: 'far fa-pen-to-square',
      show: true,
    },
    {
      tooltip: 'delete',
      class: 'far fa-trash-alt',
      show: true,
    },
  ];

  //data format for reference
  //   {
  //     "title": "sometitle",
  //     "description": "some description",
  //     "image": "image_name",
  //     "status": true
  // }

  // Grid headers
  readonly header: headerObject[] = [
    {
      title: 'Image',
      name: 'image',
      // customWidth: '200px',
      headerClass: this.imageHeaderWidth,
      type: 'html',
      dataClass: 'tableItemImg',
      formatter: ((data:any)=>{return this.renderImage(data, this.imgBaseURL)}),
      visible: true,
    },
    {
      title: 'Title',
      name: 'title',
      visible: true,
      isSortable: true,
      filter: {
        type: 'text',
        field: 'title',
        bindValue: 'title',
      },
    },
    {
      title: 'Description',
      name: 'description',
      visible: true,
      isSortable: true,
      filter: {
        type: 'text',
        field: 'description',
        bindValue: 'description',
      },
    },
    {
      title: 'Display Infront',
      name: 'displayInFront',
      isSortable: true,
      formatter: this.displayFormatter,
      dataClass: this.displayClass,
      visible: true,
      filter: {
        type: 'select',
        bindValue: 'value',
        bindLabel: 'label',
        dataKey: 'displayInfront',
        placeholder: 'Display Infront'
      },
    },
    {
      title: 'Status',
      name: 'isActive',
      isSortable: true,
      formatter: this.statusFormatter,
      dataClass: this.statusClass,
      visible: true,
      filter: {
        type: 'select',
        bindValue: 'value',
        bindLabel: 'label',
        dataKey: 'status',
        placeholder: 'Status'
      },
    },
  ];

  filterDropdownData = { status: GlobalConstants.statusData, displayInfront: GlobalConstants.displayInfront };

  constructor(
    private _router: Router,
    private _date: DateService,
    private _apiService: ApiService,
    private _toast: ToastrService,
    private _util: UtilService,
  ) {} // constructor ends here

  shellConfig: ShellConfig = null;

  ngOnInit(): void {
    this._initShellConfig();
  }

  imageHeaderWidth(){
    return 'imageWidthCustom'
  }
  // call for list data
  private _initShellConfig() {
      (this.shellConfig = {
        headers: this.header,
        endpoint: 'category/getAllCategory',
      });
  }

  statusFormatter(item) {
    if (item.isActive) {
      return 'Active';
    }
    return 'In Active';
  }

  statusClass(item) {
    if (item.isActive) {
      return 'Active';
    }
    return 'InActive';
  }

  displayFormatter(item) {
    if (item.displayInFront) {
      return 'Yes';
    }
    return 'No';
  }

  displayClass(item) {
    if (item.displayInFront) {
      return 'Yes';
    }
    return 'No';
  }

  renderImage(data:any, url:any) {
    if(data?.image){
      let fullPath = url + data.image
      return `<img src="${fullPath}" height=50 width=50>`;
    }
    else{
      return '-'
    }
  }

  goToForm() {
    this._router.navigateByUrl('master/category/form');
  }

  // click in any action in grid list
  actionButtonClick(action: any) {
    if (action.action == 'edit') {
      this._router.navigate(
        ['master/category/form/' + action.data.id],
        {
          queryParams: { isEdit: true },
        }
      );
    } else if (action.action == 'view') {
      this._router.navigate(
        ['master/category/form/' + action.data.id],
        {
          queryParams: { isView: true },
        }
      );
    } else if ((action.action = 'delete')) {
      Swal.fire({
        title: 'Are you sure',
        text: "You won't be able to revert this",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {
          this._apiService
            //soft delete so we are just updating it in the database
            .put(`category/deleteCategory/${action.data.id}`)
            .subscribe({
              next: (data) => {
                this.showLoader = false;
                this._toast.success(data.msg);
                this.gridShell.refresh();
                // this._router.navigateByUrl('/category/list');
              },
              error: (err) => {
                console.log(err,"errorrrrrrr");
                
                this.showLoader = false;
                this._toast.error(null, err.error.msg);
              },
            });
        }
      });
    }
  }
}
