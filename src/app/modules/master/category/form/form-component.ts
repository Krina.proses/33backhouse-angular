import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/core/api.service';
import { GlobalConstants } from 'src/app/services/core/global-constants';
import { CustomToastrService } from 'src/app/services/core/toastr.service';
import { UtilService } from 'src/app/services/core/util.service';

@Component({
  selector: 'app-form',
  templateUrl: './form-component.html',
  styleUrls: ['./form-component.scss'],
})
export class FormComponent implements OnInit {
  public categoryFormData: FormGroup;
  public Id: number;
  public showLoader = false;
  public submitted = false;
  public isView: boolean = false;
  public isEdit: boolean = false;
  public image;
  public imageUrl: any;
  public categoryData: any;
  public title: string = 'Add';
  imagePreview: string;


  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _apiService: ApiService,
    private _toast: CustomToastrService,
    private _util: UtilService
  ) {
    //getting id form ActivatedRoute parameteres
    this._route.params.subscribe((data: any) => {
      this.Id = data.id;
    });

    this._route.queryParams.subscribe((data) => {
      if (data['isView']) {
        this.title = 'View';
        this.isView = data['isView'];
      } else if (data['isEdit']) {
        this.title = 'Edit';
        this.isEdit = data['isEdit'];
      } else if (!data['Id']) {
      }
    });
  }
  get f(): any {
    return this.categoryFormData.controls;
  }

  goToList() {
    history.back();
  }

  //get the file name from the input and set it in the image field
  getImage(event) {
    const inputElement = event.target as HTMLInputElement;

    if (inputElement.files && inputElement.files.length> 0) {
      if (inputElement.files[0].size < GlobalConstants.fiveMBFileSize) {
        this.image = inputElement.files[0];
        this.categoryFormData.patchValue({
          image: this.image.name,
        });
        //to preview image at Add time
        if (this.image) {
          const reader = new FileReader();      
          reader.onload = (e: any) => {
            this.imagePreview = e.target.result;
          };      
          reader.readAsDataURL(this.image);
        }
      } else {
        this.categoryFormData.patchValue({
          image: null,
        });
        alert('Please Upload file less then 5MB...');
      }
    } else {
      if(!this.Id){
        this.categoryFormData.patchValue({
          image: null,
        });
        this.imagePreview = null;

      }
      alert('Please select file');
    }
  }

  formBuild() {
    this.categoryFormData = this._fb.group({
      title: [null, [Validators.required]],
      description: [null],
      image: [null, [Validators.required]],
      isActive: [true],
      displayInFront: [false],
    });
  }

  ngOnInit(): void {
    this.formBuild();
    if (this.Id) {
      this.getDatabyId();
    }
  }

  getDatabyId() {
    this._apiService.get(`category/getCategoryByID/${this.Id}`).subscribe({
      next: (data) => {
        this.categoryData = data.data;
        this.categoryFormData.patchValue({
          title: data.data.title,
          description: data.data.description,
          image: data.data.image,
          isActive: data.data.isActive,
          displayInFront: data.data.displayInFront,
        });
        if (this.isView) {
          this.categoryFormData.disable();
        }
        this.imageUrl = `${this._util.categoryImage}${data.data.image}`;
      },

      error: (err) => {
        this.showLoader = false;
        this._toast.error(null, err.error);
      },
    });
  }

  onSubmit(e) {
    this.submitted = true;

    if (this.categoryFormData.invalid) {
      return console.log('invalid');
    }
    const formData = new FormData();
    formData.append('image', this.image);
    formData.append('formData', JSON.stringify(this.categoryFormData.value));
    if (!this.isEdit) {
      this.showLoader = true;

      this._apiService.post(`category/addCategory`, formData).subscribe({
        next: (data) => {
          this.showLoader = false;
          this._router.navigateByUrl('master/category');
          this._toast.success(data.msg);
        },
        error: (err) => {          
          this.showLoader = false;
          this._toast.error(null, err.error);
        },
      });
    } else {
      this.showLoader = true;
      this._apiService
        .put(`category/updateCategory/${this.Id}`, formData)
        .subscribe({
          next: (data) => {
            this.showLoader = false;
            this._router.navigateByUrl('master/category');
            this._toast.success(data.msg);
          },
          error: (err) => {
            this.showLoader = false;
            this._toast.error(null, err.error);
          },
        });
    }
  }
}
