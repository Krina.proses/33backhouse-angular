import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/core/api.service';
import { GlobalConstants } from 'src/app/services/core/global-constants';
import { CustomToastrService } from 'src/app/services/core/toastr.service';
import { UtilService } from 'src/app/services/core/util.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  public attributeForm: FormGroup;
  public Id: number;
  public showLoader = false;
  public submitted = false;
  public isView: boolean = false;
  public isEdit: boolean = false;
  public image;
  // public imageUrl: any;
  public attributeData: any;
  public title: string = 'Add';
  imagePreview: string;
  fileArray: any = [];

  public imgBaseURL = this._util.attributeImage;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _apiService: ApiService,
    private _toast: CustomToastrService,
    private _util: UtilService
  ) {
    //getting id form ActivatedRoute parameteres
    this._route.params.subscribe((data: any) => {
      this.Id = data.id;
    });

    this._route.queryParams.subscribe((data) => {
      if (data['isView']) {
        this.title = 'View';
        this.isView = data['isView'];
      } else if (data['isEdit']) {
        this.title = 'Edit';
        this.isEdit = data['isEdit'];
      } else if (!data['Id']) {
      }
    });
  }

  ngOnInit(): void {
    this.formBuild();
    if (this.Id) {
      this.getDatabyId();
    }
  }

  get f(): any {
    return this.attributeForm.controls;
  }

  goToList() {
    history.back();
  }

  formBuild() {
    this.attributeForm = this._fb.group({
      attributeName: [null, [Validators.required]],
      displayName: [null],
      attributeDetail: this._fb.array([]),
      isActive: [true],
    });
    this.addattributeRow();
  }

  get attributeDetail() {
    return this.attributeForm.get('attributeDetail') as FormArray;
  }

  setattributeDetail(list: any[]) {
    if (list.length !== 0) {
      const fg = list.map((i) => this._fb.group(i));
      const fa = this._fb.array(fg);

      this.attributeForm.setControl('attributeDetail', fa);
    }
  }

  patchattributeDetailList(list: any[]) {
    this.attributeForm.patchValue({
      attributeDetail: list,
    });
  }

  addattributeRow() {
    const obj = this._fb.group({
      value: [null, [Validators.required]],
      description: [null],
      image: [null],
      isActive: [true],
    });

    this.attributeDetail.push(obj);
  }

  removeattributeRow(i: number, item: any) {
    
    this._apiService.delete(`attribute/deleteAttributeDetail/${item.id}`).subscribe({
      next: (data) => {
        this.showLoader = false;
        this.attributeDetail.removeAt(i);
      },
      error: (err) => {        
        this.showLoader = false;
        this._toast.error(null, err.error.msg);
      },
    });
  }

  onFileChanged(event, index?) {
    if (!event.target.files.length) {
      alert('Please select file');
      return;
    }

    let myFile = event.target.files[0];

    if (myFile.size > GlobalConstants.fiveMBFileSize) {
      event.target.value = '';
      alert('Please Upload file lessthen 5 MB...');
      return;
    }

    if (
      myFile.name.includes('png') ||
      myFile.name.includes('jpg') ||
      myFile.name.includes('jpeg')
    ) {
    } else {
      event.target.value = '';
      alert('Please Select Image File - .png, .jpg, .jpeg');
      return;
    }

    // if (myFile.type === 'image/png' || myFile.type === 'image/jpg' || myFile.type === 'image/jpeg') {
    // } else {
    //   event.target.value = '';
    //   this._toast.warning("Please Select Image File (ex.- .png, .jpg, .jpeg)");
    // }

    if (myFile.name) {
      this.attributeForm.controls.attributeDetail['controls'][index].controls[
        'image'
      ].setValue(myFile.name);
    } else {
      this.attributeForm.controls.attributeDetail['controls'][index].controls[
        'image'
      ].setValue(null);
    }
    this.fileArray[index] = event.target.files[0];
  }

  getDatabyId() {
    this._apiService.get(`attribute/getAttributeById/${this.Id}`).subscribe({
      next: (data) => {
        // return
        console.log(data.data,"data.data");
        
        this.attributeData = data.data;
        this.attributeForm.patchValue({
          attributeName: data.data.attributeName,
          displayName: data.data.displayName,
          attributeDetail: data.data.AttributeDetails,
          isActive: data.data.isActive,
        });
        
        this.setattributeDetail(data.data.AttributeDetails || []);
        if (this.isView) {
          this.attributeForm.disable();
        }
      },

      error: (err) => {
        this.showLoader = false;
        this._toast.error(null, err.error);
      },
    });
  }

  onSubmit(e) {
    this.submitted = true;

    if (this.attributeForm.invalid) {
      return console.log('invalid');
    }

    let formValues = this.attributeForm.value;
    
    let formData = new FormData();

    let files = this.attributeForm.value.attributeDetail;
    let index = 0;
    let length = files.length;
    while (index < length) {
      formData.append('FILES', this.fileArray[index]);
      index++;
    }
    formData.append('formData', JSON.stringify(formValues));

    if (!this.isEdit) {
      this.showLoader = true;

      this._apiService.post(`attribute/addAttribute`, formData).subscribe({
        next: (data) => {
          this.showLoader = false;
          this._router.navigateByUrl('master/attribute');
          this._toast.success(data.msg);
        },
        error: (err) => {
          console.log(err,"errorrrrrrrrrr");
          
          this.showLoader = false;
          this._toast.error(null, err.error);
        },
      });
    } else {
      this.showLoader = true;
      this._apiService
        .put(`attribute/updateAttribute/${this.Id}`, formData)
        .subscribe({
          next: (data) => {
            this.showLoader = false;
            this._router.navigateByUrl('master/attribute');
            this._toast.success(data.msg);
          },
          error: (err) => {
            this.showLoader = false;
            this._toast.error(null, err.error);
          },
        });
    }
  }
}
