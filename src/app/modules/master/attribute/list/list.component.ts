import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/core/api.service';
import { DateService } from 'src/app/services/core/date.service';
import { GlobalConstants } from 'src/app/services/core/global-constants';
import {
  actionButtonType,
  headerObject,
} from 'src/app/shared/ui/dash-data-grid/data-grid.type';
import { GridShellComponent } from 'src/app/shared/ui/data-grid-shell/grid-shell.component';
import { ShellConfig } from 'src/app/shared/ui/data-grid-shell/grid-shell.types';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  @ViewChild(GridShellComponent)
  public gridShell: GridShellComponent;
  public showLoader: boolean;

  //Action buttons
  readonly actionBtn: actionButtonType[] = [
    {
      tooltip: 'view',
      class: 'far fa-eye',
      show: true,
    },
    {
      tooltip: 'edit',
      class: 'far fa-pen-to-square',
      show: true,
    },
    {
      tooltip: 'delete',
      class: 'far fa-trash-alt',
      show: true,
    },
  ];


  // Grid headers
  readonly header: headerObject[] = [
    {
      title: 'Attribute',
      name: 'attributeName',
      visible: true,
      isSortable: true,
      filter: {
        type: 'text',
        field: 'attributeName',
        bindValue: 'attributeName',
      },
    },
    {
      title: 'Display Name',
      name: 'displayName',
      visible: true,
      isSortable: true,
      filter: {
        type: 'text',
        field: 'displayName',
        bindValue: 'displayName',
      },
    },
    {
      title: 'Status',
      name: 'isActive',
      isSortable: true,
      formatter: this.statusFormatter,
      dataClass: this.statusClass,
      visible: true,
      filter: {
        type: 'select',
        bindValue: 'value',
        bindLabel: 'label',
        dataKey: 'status',
        placeholder: 'Status',
      },
    },
  ];

  filterDropdownData = {
    status: GlobalConstants.statusData,
  };

  constructor(
    private _router: Router,
    private _date: DateService,
    private _apiService: ApiService,
    private _toast: ToastrService
  ) {} // constructor ends here

  shellConfig: ShellConfig = null;

  ngOnInit(): void {
    this._initShellConfig();
  }

  // call for list data
  private _initShellConfig() {
    this.shellConfig = {
      headers: this.header,
      endpoint: 'attribute/getAllAttributeFilterWise',
    };
  }

  statusFormatter(item) {
    if (item.isActive) {
      return 'Active';
    }
    return 'In Active';
  }

  statusClass(item) {
    if (item.isActive) {
      return 'Active';
    }
    return 'InActive';
  }

  displayFormatter(item) {
    if (item.displayInFront) {
      return 'Yes';
    }
    return 'No';
  }

  displayClass(item) {
    if (item.displayInFront) {
      return 'Yes';
    }
    return 'No';
  }

  goToForm() {
    this._router.navigateByUrl('master/attribute/form');
  }

  // click in any action in grid list
  actionButtonClick(action: any) {
    if (action.action == 'edit') {
      this._router.navigate(
        ['master/attribute/form/' + action.data.id],
        {
          queryParams: { isEdit: true },
        }
      );
    } else if (action.action == 'view') {
      this._router.navigate(
        ['master/attribute/form/' + action.data.id],
        {
          queryParams: { isView: true },
        }
      );
    } else if ((action.action = 'delete')) {
      Swal.fire({
        title: 'Are you sure',
        text: "You won't be able to revert this",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
      }).then((result) => {
        if (result.value) {
          this._apiService
            .delete(`attribute/deleteAttribute/${action.data.id}`)
            .subscribe({
              next: (data) => {
                this.showLoader = false;
                this._toast.success(data.msg);
                this.gridShell.refresh();
                // this._router.navigateByUrl('/category/list');
              },
              error: (err) => {
                this.showLoader = false;
                this._toast.error(null, err.error.msg);
              },
            });
        }
      });
    }
  }
}
