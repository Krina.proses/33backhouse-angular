import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AttributeRoutingModule } from './attribute-routing.module';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    FormComponent,
    ListComponent
  ],
  imports: [
    CommonModule,
    AttributeRoutingModule,
    NgSelectModule,
    SharedModule,
    ReactiveFormsModule,
  ]
})
export class AttributeModule { }
