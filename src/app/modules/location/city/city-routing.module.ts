import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [
  {
    path:'', component:ListComponent
  },
  {
    path:'form', component:FormComponent
  },
  {
    path: 'form/:id',
    component: FormComponent,
  },
  {
    path:'list', component:ListComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CityRoutingModule { }
