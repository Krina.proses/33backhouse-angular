import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/core/api.service';
import { CustomToastrService } from 'src/app/services/core/toastr.service';
import { UtilService } from 'src/app/services/core/util.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  cityForm: FormGroup;

  //types
  Id: number;
  countryList = [];
  country: any = [];
  stateList = [];
  state: any = [];
  submit: boolean = false;
  public showLoader = false;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _apiService: ApiService,
    private _toast: CustomToastrService,
    private _util: UtilService
  ) {
      // get id from ActivatedRoute params
      this._route.params.subscribe((data: any) => {
        this.Id = data.id;
      });
   }

  ngOnInit(): void {
    this.builderform();
    this.getCountry();
    if (this.Id) {
      this.getDatabyId();
    }
  }

  builderform() {
    this.cityForm = this._fb.group({
      countryId: [null, [Validators.required]],
      stateId: [null, [Validators.required]],
      cityName: [null, [Validators.required]],
      stdCode: [null],
      isActive: [true],
    });
  }


  getDatabyId() {
    this._apiService.get(`city/getCitybyID/${this.Id}`).subscribe(
      (data) => {
        this.getState(data.data['state.country.id']);
        this.cityForm.patchValue({
          countryId: data.data['state.country.id'],
          stateId: data.data.stateId,
          cityName: data.data.cityName,
          stdCode: data.data.stdCode,
          isActive: data.data.isActive,
        });
      },
      (err) => {
        this.showLoader = false;
        this._toast.error(null, err.error);
      }
    );
  }

  get g() {
    return this.cityForm.controls;
  }

    //get country by dropdown
    getCountry() {
      try {
        this._apiService.get(`country/getAllActiveCountry`).subscribe(
          (data) => {
            this.countryList = data.data;
            console.log(data.data, 'this is plantList data');
          }
        );
      } catch (error) {
        this._toast.error(null, error.error);
      }
    }

    setCountry(e: any) {
      console.log(e, "stateidddd");
      this.getState(e.id);
      this.cityForm.patchValue({
        // stateId: null,
        stateId: null
      });
    }

        //get state by dropdown
        getState(e: any) {
          try {
            let id = e ;
            this._apiService.get(`state/getAllActiveState/${id}`).subscribe(
              (data) => {
                this.stateList = data.data;
                console.log(data.data, 'this is plantList data');
              }
            );
          } catch (error) {
            this._toast.error(null, error.error);
          }
        }

        setState() {}


        onSubmit(e) {
          this.submit = true;

          if (this.cityForm.invalid) {
            return console.log('invalid');
          } else {
            if (!this.Id) {
              this.showLoader = true;
              this._apiService
                .post(`city/addCity`, this.cityForm.getRawValue())
                .subscribe(
                  (data) => {
                    this.showLoader = false;
                    this._router.navigate(['/location/city/list']);
                    this._toast.success(data.msg);
                  },
                  (err) => {
                    this.showLoader = false;
                    this._toast.error(null, err.error);
                  }
                );
            } else {
              this.showLoader = true;
              this._apiService
                .put(`city/updateCity/${this.Id}`, {
                  ...this.cityForm.getRawValue()
                })
                .subscribe(
                  (data) => {
                    this.showLoader = false;
                    this._router.navigate(['/location/city/list']);
                    this._toast.success(data.msg);
                  },
                  (err) => {
                    this.showLoader = false;
                    this._toast.error(null, err.error);
                  }
                );
            }
          }
        }

         //back to list
  goToList() {
    history.back();
  }

}
