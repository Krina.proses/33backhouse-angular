import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/core/api.service';
import { GlobalConstants } from 'src/app/services/core/global-constants';
import { CustomToastrService } from 'src/app/services/core/toastr.service';
import {
  actionButtonType,
  headerObject,
} from 'src/app/shared/ui/dash-data-grid/data-grid.type';
import { ShellConfig } from 'src/app/shared/ui/data-grid-shell/grid-shell.types';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  // variable declaration starts here
  actionClickedId!: string;
  // userList$;
  showLoader = false;
  showAddBtn!: Boolean;
  addBtn = false;
  // hodUserList = [];
  // variables for grid filter
  totalItems: number = 0;
  currentPage: any;
  itemsPerPage!: Number;
  filterStart: Boolean = true;

  //Action buttons
  readonly actionBtn: actionButtonType[] = [
    {
      tooltip: 'view',
      class: 'far fa-eye',
      show: true,
    },
    {
      tooltip: 'edit',
      class: 'far fa-pen-to-square',
      show: true,
    },
  ];

  // Grid headers
  readonly header: headerObject[] = [
    {
      title: 'Country',
      name: 'countryName',
      visible: true,
      isSortable: true,
      filter: {
        type: 'text',
        field: 'countryName',
        bindValue: 'countryName',
      },
    },
    {
      title: 'ISO Code',
      name: 'isoCode',
      visible: true,
      isSortable: true,
      filter: {
        type: 'text',
        field: 'isoCode',
        bindValue: 'isoCode',
      },
    },
    {
      title: 'ISD Code',
      name: 'isdCode',
      visible: true,
      isSortable: true,
      filter: {
        type: 'text',
        field: 'isdCode',
        bindValue: 'isdCode',
      },
    },
    {
      title: 'Status',
      name: 'isActive',
      isSortable: true,
      formatter: this.statusFormatter,
      dataClass: this.statusClass,
      visible: true,
      filter: {
        type: 'select',
        bindValue: 'value',
        bindLabel: 'label',
        dataKey: 'status',
        placeholder: 'status',
      },
    },
  ];

  filterDropdownData = { status: GlobalConstants.statusData };

  constructor(
    private _router: Router,
    private _apiService: ApiService,
    private _toast: CustomToastrService
  ) {
    let url = _router.routerState.snapshot.url;
  }

  shellConfig: ShellConfig = null;

  ngOnInit(): void {
    this._initShellConfig();
  }

  // call for list data
  private _initShellConfig() {
    this.shellConfig = {
      headers: this.header,
      endpoint: 'country/getAllCountry',
    };
  }

  statusFormatter(item) {
    if (item.isActive) {
      return 'Active';
    }
    return 'In Active';
  }

  statusClass(item) {
    if (item.isActive) {
      return 'Active';
    }
    return 'InActive';
  }

  goToForm() {
    this._router.navigateByUrl('location/country/form');
  }

  // click in any action in grid list
  actionButtonClick(action: any) {
    if (action.action == 'edit') {
      this._router.navigate(['location/country/form/' + action.data.id], {
        queryParams: { isEdit: true },
      });
    } else if (action.action == 'view') {
      this._router.navigate(['location/country/form/' + action.data.id], {
        queryParams: { isView: true },
      });
    }
  }

}
