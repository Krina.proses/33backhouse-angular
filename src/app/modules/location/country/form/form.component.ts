import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/core/api.service';
import { CustomToastrService } from 'src/app/services/core/toastr.service';
import { UtilService } from 'src/app/services/core/util.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  countryForm: FormGroup;
  //types
  Id: number;
  submit: boolean = false;
  public showLoader = false;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _apiService: ApiService,
    private _toast: CustomToastrService,
    private _util: UtilService
  ) {
    // get id from ActivatedRoute params
    this._route.params.subscribe((data: any) => {
      this.Id = data.id;
    });
  }

  ngOnInit(): void {
    this.builderform();
    if (this.Id) {
      this.getDataByID();
    }
  }

  builderform() {
    this.countryForm = this._fb.group({
      countryName: [null, [Validators.required]],
      isoCode: [null],
      isdCode: [null],
      isActive: [true],
    });
  }

  //getdata by id
  getDataByID() {
    try {
      this._apiService.get(`country/getCountrybyID/${this.Id}`).subscribe(
        (data) => {
        console.log(data.data, "patch data");

        this.countryForm.patchValue({
        countryName: data.data.countryName,
        isoCode: data.data.isoCode,
        isdCode: data.data.isdCode,
        isActive: data.data.isActive,
      });
      })
    } catch (error) {
      this.showLoader = false;
        this._toast.error(null, error.error);
    }
  }

  get g() {
    return this.countryForm.controls;
  }


   //add and update api
   onSubmit(e) {
    this.submit = true;

    if (this.countryForm.invalid) {
      return console.log('invalid');
    } else {
      if (!this.Id) {
        this.showLoader = true;
        this._apiService
          .post(`country/addCountry`, this.countryForm.getRawValue())
          .subscribe(
            (data) => {
              this.showLoader = false;
              this._router.navigate(['/location/country/list']);
              this._toast.success(data.msg);
            },
            (err) => {
              this.showLoader = false;
              this._toast.error(null, err.error);
            }
          );
      } else {
        this.showLoader = true;
        this._apiService
          .put(`country/updateCountry/${this.Id}`, {
            ...this.countryForm.getRawValue(),
            id: this.Id,
          })
          .subscribe(
            (data) => {
              this.showLoader = false;
              this._router.navigate(['/location/country/list']);
              this._toast.success(data.msg);
            },
            (err) => {
              this.showLoader = false;
              this._toast.error(null, err.error);
            }
          );
      }
    }
  }

  //back to list
  goToList() {
    history.back();
  }

}
