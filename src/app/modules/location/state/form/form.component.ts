import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/core/api.service';
import { CustomToastrService } from 'src/app/services/core/toastr.service';
import { UtilService } from 'src/app/services/core/util.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  stateForm: FormGroup;

  //types
  Id: number;
  countryList = [];
  country: any = [];
  submit: boolean = false;
  public showLoader = false;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _apiService: ApiService,
    private _toast: CustomToastrService,
    private _util: UtilService
  ) {
    // get id from ActivatedRoute params
    this._route.params.subscribe((data: any) => {
      this.Id = data.id;
    });
  }

  ngOnInit(): void {
    this.builderform();
    if (this.Id) {
      this.getDataByID();
    }
    this.getCountry();

  }

  builderform() {
    this.stateForm = this._fb.group({
      countryId: [null, [Validators.required]],
      stateName: [null, [Validators.required]],
      stateCode: [null, [Validators.required]],
      isActive: [true],
    });
  }

  //getdata by id
  getDataByID() {
    try {
      this._apiService.get(`state/getStatebyID/${this.Id}`).subscribe(
        (data) => {
          console.log(data.data, "patch data");
          this.stateForm.patchValue({
            countryId: data.data.countryId,
            stateName: data.data.stateName,
            stateCode: data.data.stateCode,
            isActive: data.data.isActive,
          });
        })
    } catch (error) {
      this.showLoader = false;
      this._toast.error(null, error.error);
    }
  }

  get g() {
    return this.stateForm.controls;
  }

  //get country by dropdown
  getCountry() {
    try {
      this._apiService.get(`country/getAllActiveCountry`).subscribe(
        (data) => {
          this.countryList = data.data;
          console.log(data.data, 'this is plantList data');
        }
      );
    } catch (error) {
      this._toast.error(null, error.error);
    }
  }

  setCountry() {}


  //add and update api
  onSubmit(e) {
    this.submit = true;

    if (this.stateForm.invalid) {
      return console.log('invalid');
    } else {
      if (!this.Id) {
        this.showLoader = true;
        this._apiService
          .post(`state/addState`, this.stateForm.getRawValue())
          .subscribe(
            (data) => {
              this.showLoader = false;
              this._router.navigate(['/location/state/list']);
              this._toast.success(data.msg);
            },
            (err) => {
              this.showLoader = false;
              this._toast.error(null, err.error);
            }
          );
      } else {
        this.showLoader = true;
        this._apiService
          .put(`state/updateState/${this.Id}`, {
            ...this.stateForm.getRawValue()
          })
          .subscribe(
            (data) => {
              this.showLoader = false;
              this._router.navigate(['/location/state/list']);
              this._toast.success(data.msg);
            },
            (err) => {
              this.showLoader = false;
              this._toast.error(null, err.error);
            }
          );
      }
    }
  }



  //back to list
  goToList() {
    history.back();
  }

}
