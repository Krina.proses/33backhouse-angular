import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [

  {
    path: 'city',
    loadChildren: () => import('./city/city.module').then((m) => m.CityModule),
  },
  {
    path: 'state',
    loadChildren: () => import('./state/state.module').then((m) => m.StateModule),
  },
  {
    path: 'country',
    loadChildren: () => import('./country/country.module').then((m) => m.CountryModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationRoutingModule { }
