import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';
import { headerObject } from 'src/app/shared/ui/dash-data-grid/data-grid.type';

type exportInfoType = {
  url: string;
  filename: string;
  filters: any;
  headers: headerObject[];
  extraExportKeys: { key: string; title: string }[];
  allSelected?:boolean, excluded?:any[], included?:any[]
};

@Injectable({
  providedIn: 'root',
})
export class ExcelService {
  private exportInfo = new BehaviorSubject<exportInfoType>({
    url: '',
    filename: '',
    filters: {},
    headers: [],
    extraExportKeys: [],
  });
  exportInfo$ = this.exportInfo.asObservable();
  updateExportInfo(data: exportInfoType) {
    this.exportInfo.next({ ...data });
  }

}
