import { Location } from '@angular/common';
import { Component, NgModule, OnDestroy, OnInit } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApiService } from 'src/app/services/core/api.service';
import { headerObject } from 'src/app/shared/ui/dash-data-grid/data-grid.type';
import { SubSink } from 'subsink';
import * as XLSX from 'xlsx';
import { ExcelService } from './excel.service';

@Component({
  selector: 'excel-component',
  template: `<p>Loading...</p>`,
})
export class ExcelComponent implements OnInit, OnDestroy {
  constructor(
    private api: ApiService,
    private es: ExcelService,
    private location: Location
  ) { }
  private subs = new SubSink();
  ngOnInit() {
    this.subs.sink = this.es.exportInfo$.subscribe((info) => {
      if (!info.url) return;
      this.api.get(info.url, {...info.filters, allSelected: info.allSelected, included: info.included, excluded: info.excluded }).subscribe({
        next: ({ data }) => {
          if (data && data.length) {
            this.exportExcel(
              this.formatData(info.headers, data, info.extraExportKeys),
              info.filename
            );
          }
        },
        error: () => this.location.back(),
      });
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  private formatData(
    headers: headerObject[],
    data: any[],
    exportKeys: { key: string; title: string }[]
  ) {
    let result = [];
    for (let d of data) {
      let obj = {};
      for (let h of headers) {
        if (h.visible) {
          obj[h.title] = h.formatter ? h.formatter(d, h) : d[h.name];
        }
      }
      if(exportKeys) {
        for (let e of exportKeys) {
          obj[e.title] = d[e.key];
        }
      }

      result.push(obj);
    }
    return result;
  }

  private exportExcel(json: any[], fileName: string = 'excel'): void {
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, fileName);
    XLSX.writeFile(wb, `${fileName}.xlsx`);
    this.location.back();
  }
}

const routes: Routes = [
  {
    path: '',
    component: ExcelComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExcelRoutingModule { }

@NgModule({
  declarations: [ExcelComponent],
  exports: [ExcelComponent],
  imports: [ExcelRoutingModule],
})
export class ExcelModule { }
